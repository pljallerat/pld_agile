package model;

import java.util.Date;

/**
 * Model class representing the depot (departure of a planning request)
 */
public class Depot {
    private Date departureTime;
    private Intersection address;

    public Depot(Date departureTime, Intersection address) {
        this.departureTime = departureTime;
        this.address = address;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Intersection getAddress() {
        return address;
    }

    public void setAddress(Intersection address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Depot{" +
                "departureTime=" + departureTime +
                ", address=" + address +
                '}';
    }
}
