package model;

import java.util.Set;

/**
 * Class representing a map, containing a set of every available segments
 * @see Segment
 */
public class Map {
    private Set<Segment> segments;

    public Map(Set<Segment> segments) {
        this.segments = segments;
    }

    public Set<Segment> getSegments() {
        return segments;
    }

    public void setSegments(Set<Segment> segments) {
        this.segments = segments;
    }

}
