package model;

/**
 * A delivery request, containing a pickup that must be reach before the delivery
 */
public class Request {
    private int pickupDuration;
    private int deliveryDuration;
    private Intersection pickupAddress;
    private Intersection deliveryAddress;

    public Request(int pickupDuration, int deliveryDuration, Intersection pickupAddress, Intersection deliveryAddress) {
        this.pickupDuration = pickupDuration;
        this.deliveryDuration = deliveryDuration;
        this.pickupAddress = pickupAddress;
        this.deliveryAddress = deliveryAddress;
    }

    public int getPickupDuration() {
        return pickupDuration;
    }

    public void setPickupDuration(int pickupDuration) {
        this.pickupDuration = pickupDuration;
    }

    public int getDeliveryDuration() {
        return deliveryDuration;
    }

    public void setDeliveryDuration(int deliveryDuration) {
        this.deliveryDuration = deliveryDuration;
    }

    public Intersection getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(Intersection pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public Intersection getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Intersection deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Override
    public String toString() {
        return "Request{" +
                "pickupDuration=" + pickupDuration +
                ", deliveryDuration=" + deliveryDuration +
                ", pickupAddress=" + pickupAddress +
                ", deliveryAddress=" + deliveryAddress +
                '}';
    }
}
