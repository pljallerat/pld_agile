package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a planning request
 * Containing a start {@link Depot} and delivery requests to satisfy
 * @see PlanningRequest
 */
public class PlanningRequest {
    private Depot depot;
    private List<Request> requests;

    public PlanningRequest(Depot depot, List<Request> requests) {
        this.depot = depot;
        this.requests = requests;
    }

    public PlanningRequest(PlanningRequest planningRequest) {
        this.depot = planningRequest.depot;
        this.requests = new ArrayList<>(planningRequest.requests);
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    public List<Request> getRequests() {
        return requests;
    }

    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    @Override
    public String toString() {
        return "PlanningRequest{" + depot +
                ", request list: " + requests +
                '}';
    }
}
