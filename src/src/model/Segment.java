package model;

/**
 * A one-direction connexion between tow {@link Intersection}, containing its length
 */
public class Segment {
    private float length;
    private String name;
    private Intersection origin;
    private Intersection destination;

    public Segment(float length, String name, Intersection origin, Intersection destination) {
        this.length = length;
        this.name = name;
        this.origin = origin;
        this.destination = destination;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Intersection getOrigin() {
        return origin;
    }

    public void setOrigin(Intersection origin) {
        this.origin = origin;
    }

    public Intersection getDestination() {
        return destination;
    }

    public void setDestination(Intersection destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Segment{" +
                "length=" + length +
                ", name='" + name + '\'' +
                ", origin=" + origin +
                ", destination=" + destination +
                '}';
    }
}

