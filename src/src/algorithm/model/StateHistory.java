package algorithm.model;

import java.util.LinkedList;

/**
 * Object managing state history, for undo and redo
 */
public class StateHistory {
    private LinkedList<State> history;
    private int currentStateIndex;

    /**
     * Constructor that initialize an empty history
     */
    public StateHistory() {
        history = new LinkedList<>();
        currentStateIndex = -1;
    }

    /**
     * Clear the history of updates
     * (For examples when a new map/request is loaded)
     */
    public void clearHistory() {
        history.clear();
        currentStateIndex = -1;
    }

    /**
     * @return if at least one state is stored
     */
    public boolean hasState() {
        return currentStateIndex >= 0;
    }

    /**
     * add a state to the state history
     *
     * @param newState new state, current
     */
    public void addModification(State newState) {
        while (currentStateIndex > 0) {
            history.removeFirst();
            currentStateIndex--;
        }
        history.addFirst(newState);
        currentStateIndex = 0;
    }

    /**
     * Undo last modification
     * Update active state
     */
    public void undo() {
        if (currentStateIndex < history.size() - 1) {
            currentStateIndex++;
        }
    }

    /**
     * Redo last un-done modification
     * Update active state
     */
    public void redo() {
        if (currentStateIndex > 0) {
            currentStateIndex--;
        }
    }

    /**
     * @return the current active state
     */
    public State getCurrentState() {
        if (hasState()) {
            return history.get(currentStateIndex);
        }
        return null;
    }
}
