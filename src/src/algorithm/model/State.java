package algorithm.model;

import javafx.util.Pair;
import model.Intersection;
import model.PlanningRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * A state of the application
 *
 * used to store and restore states with undo/redo functionality
 */
public class State {
    public final List<Intersection> path;
    public final List<Pair<Long, Intersection>> arrivalTimes;
    public final List<Intersection> orderedListOfUsefulIntersections;
    public final PlanningRequest planningRequest;

    /**
     * Constructor that initialize values, by copying parameters content in order to make those immutable
     *
     * @param path Order list of every visited intersections (with useful one duplicated)
     * @param arrivalTimes arrival times at useful intersections (pickups + deliveries + depot)
     * @param orderedListOfUsefulIntersections ordered list of the useful intersections
     * @param planningRequest Corresponding planning request
     * @see PlanningRequest
     */
    public State(List<Intersection> path, List<Pair<Long, Intersection>> arrivalTimes,
                 List<Intersection> orderedListOfUsefulIntersections, PlanningRequest planningRequest) {
        this.path = path == null ? null : new ArrayList<>(path);
        this.arrivalTimes = arrivalTimes == null ? null : new ArrayList<>(arrivalTimes);
        this.orderedListOfUsefulIntersections =
                orderedListOfUsefulIntersections == null ? null :
                        new ArrayList<>(orderedListOfUsefulIntersections);
        this.planningRequest = new PlanningRequest(planningRequest);
    }
}
