package algorithm.model;

import model.Intersection;

import java.util.HashMap;
import java.util.Map;

public interface Graph {
    /**
     * @return the number of vertices in <code>this</code>
     */
    int getNbVertices();

    /**
     * @param i
     * @param j
     * @return the cost of arc (i,j) if (i,j) is an arc; -1 otherwise
     */
    float getCost(int i, int j);

    /**
     * @param i
     * @param j
     * @return true if <code>(i,j)</code> is an arc of <code>this</code>
     */
    boolean isArc(int i, int j);

    /**
     * return rules of precedence that needs to be respected (pickup before delivery)
     * @return map linking needed origin (key) to destination (value) (pickup -> delivery)
     */
    Map<Integer,Integer> getPrecedence();

    /**
     * return the adjacent matrice of the graph
     * @return 2DArray of float
     */
    float[][] getMatrice();

    /**
     * return the intersection to an index
     * @param i The index in the tsp code
     * @return The Intersection
     */
    Intersection getIntersectionFromIndex(int i);

}
