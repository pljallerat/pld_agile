/*
 * Class by Antoine MANDIN
 *
 * Wrote on 17-11-2020
 */

package algorithm.model;

import model.Intersection;

import java.util.HashMap;
import java.util.Map;

/**
 * Class representing a Vertex into a graph.
 * Used to compute shorter path finding
 *
 * @see algorithm.spf.Dijkstra
 * @see algorithm.spf.MapPathFinder
 */
public class Vertex implements Comparable<Vertex> {
    /**
     * Adjacency list, contains Vertex linked to
     */
    Map<Vertex, Float> adjacencyList;

    /**
     * Predecessor Vertex used by the shorter path
     *
     * @see algorithm.spf.Dijkstra#start(int)
     */
    Vertex predecessor;

    /**
     * Current shorter distance from origin
     *
     * @see algorithm.spf.Dijkstra#start(int)
     */
    float currentDistance;

    final Intersection intersection;

    public Vertex(Intersection intersection) {
        this.intersection = intersection;
        adjacencyList = new HashMap<>();
        currentDistance = Float.POSITIVE_INFINITY;
    }

    /**
     * Add a neighbor the the adjacency list
     *
     * @param neighbor Vertex neighbor to this vertex
     * @param length   distance between this vertex and its neighbor
     */
    public void addNeighbor(Vertex neighbor, float length) {
        adjacencyList.put(neighbor, length);
    }

    /**
     * Set nearer predecessor
     *
     * @param predecessor     new nearer predecessor
     * @param currentDistance new shorted distance
     */
    public void setPredecessor(Vertex predecessor, float currentDistance) {
        this.predecessor = predecessor;
        this.currentDistance = currentDistance;
    }

    /**
     * Function used to sort Vertices
     * sorting Vertices the nearer to the farthest
     *
     * @param o object to compare with
     * @return integer comparing Vertices : negative if "this < o", positive if "this > o" else "this = o"
     * @see Float#compare(float, float)
     */
    @Override
    public int compareTo(Vertex o) {
        return Float.compare(this.currentDistance, o.currentDistance);
    }

    public void setDistance(float distance) {
        this.currentDistance = distance;
    }

    public Map<Vertex, Float> getAdjacencyList() {
        return adjacencyList;
    }

    public Vertex getPredecessor() {
        return predecessor;
    }

    public float getCurrentDistance() {
        return currentDistance;
    }

    public Intersection getIntersection() {
        return intersection;
    }
}
