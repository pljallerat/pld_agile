/*
 * Class by Antoine MANDIN
 *
 * Wrote on 17-11-2020
 */

package algorithm.model;

import model.Intersection;

import java.util.List;

/**
 * Class representing a path between to intersection, passing through several intersections
 *
 * @see model.Intersection
 * @see algorithm.spf.MapPathFinder#findShorterPaths()
 */
public class Path {
    // Ordered list of segment of the path
    private List<Intersection> vertices;

    // Total length of the path
    private float totalLength;

    /**
     * Constructor with every variables
     *
     * @param vertices ordered list of every visited intersection
     * @param totalLength whole length of the path
     */
    public Path(List<Intersection> vertices, float totalLength) {
        this.vertices = vertices;
        this.totalLength = totalLength;
    }

    public List<Intersection> getVertices() {
        return vertices;
    }

    public float getTotalLength() {
        return totalLength;
    }

}
