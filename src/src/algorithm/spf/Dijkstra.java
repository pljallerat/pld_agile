/*
 * Class by Antoine MANDIN
 *
 * Wrote on 17-11-2020
 */

package algorithm.spf;

import algorithm.model.Vertex;
import model.Intersection;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Dijkstra class used to implement the "Shorter Path Finding" algorithm Dijkstra
 */
public class Dijkstra {
    /**
     * Array of Vertex,
     * using Adjacency list
     * updating Predecessor and Distance
     *
     * @see algorithm.model.Vertex
     */
    protected Vertex[] vertices;

    /**
     * Index of the Starting vertex into {@link Dijkstra#vertices}
     */
    protected int startVertexIndex = -1;

    /**
     * Set of visited Vertices
     */
    protected Set<Vertex> visited;

    /**
     * Set of visiting vertices
     * Sorted from the nearest to the farthest
     */
    protected SortedSet<Vertex> visiting;

    /**
     * Constructor with vertices list
     * @param vertices list of {@link Vertex}
     */
    public Dijkstra(Vertex[] vertices) {
        this.vertices = vertices;
    }

    /**
     * Start the algorithm
     *
     * @param startVertexIndex index of the vertex to take as a starting point
     */
    public void start(int startVertexIndex) {
        this.startVertexIndex = startVertexIndex;
        init();
        proceed();
    }

    /**
     * Used to init the algorithm values
     */
    protected void init() {
        //System.out.println("Disjktra: init");
        resetVertices();
        vertices[startVertexIndex].setDistance(0);

        if (visiting == null) {
            visiting = new ConcurrentSkipListSet<>();
        } else {
            visiting.clear();
        }
        visiting.add(vertices[startVertexIndex]);

        if (visited == null) {
            visited = new HashSet<>();
        } else {
            visited.clear();
        }
    }

    /**
     * Used to reset {@link Vertex#predecessor} and {@link Vertex#currentDistance} values
     */
    protected void resetVertices() {
        for (Vertex vertex : vertices) {
            vertex.setPredecessor(null, Float.POSITIVE_INFINITY);
        }
    }

    public Vertex[] getVertices(){
        return vertices;
    }

    /**
     * Actually launch the algorithm
     */
    protected void proceed() {
        while (!visiting.isEmpty()) {
            Vertex current = ((ConcurrentSkipListSet<Vertex>) visiting).pollFirst();
            visited.add(current);
            for (Vertex neighbor : current.getAdjacencyList().keySet()) {
                if (visited.contains(neighbor)) {
                    continue;
                }
                float newDistance;
                if (neighbor.getCurrentDistance() > (newDistance = current.getCurrentDistance()
                        + current.getAdjacencyList().get(neighbor))) {
                    neighbor.setPredecessor(current, newDistance);
                }
                // If the vertex wasn't already in the visiting set, we add it
                visiting.add(neighbor);
            }
        }
    }
}
