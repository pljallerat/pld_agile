/*
 * Class by Antoine MANDIN
 *
 * Wrote on 17-11-2020
 */

package algorithm.spf;

import algorithm.model.Path;
import algorithm.model.Vertex;
import javafx.util.Pair;
import model.*;
import model.Map;
import model.Intersection;

import java.util.*;

/**
 * Class used to find shorted paths into a {@link model.Map} linking
 * {@link model.PlanningRequest} key Intersections
 */
public class MapPathFinder {
    private static Set<Intersection> usefulIntersections;

    /** Map from {@link model.Intersection#id} to {@link Vertex} */
    private java.util.Map<Long, Vertex> verticesMap;

    public java.util.Map<Long,Vertex> getVerticesMap() {
        return verticesMap;
    }

    /** Map from Pair of tow {@link Intersection} to {@link Path} */
    private java.util.Map<Pair<Intersection, Intersection>, Path> reducedGraph;


    /**
     * Build structure needed to computing shorter paths
     *
     * @param map the whole map of every intersection and segments ({@link Map})
     * @param planningRequest the planning request to satisfy
     */
    public MapPathFinder(Map map, PlanningRequest planningRequest) {
        //System.out.println("MapPathFinder: <init> ");
        usefulIntersections = new HashSet<>();
        usefulIntersections.add(planningRequest.getDepot().getAddress());
        for (Request request : planningRequest.getRequests()) {
            usefulIntersections.add(request.getPickupAddress());
            usefulIntersections.add(request.getDeliveryAddress());
        }


        // We need to build whole adjacency lists from Map object
        verticesMap = new HashMap<>();

        for (Segment segment : map.getSegments()) {
            Vertex origin = putOrGet(verticesMap,segment.getOrigin());
            Vertex destination = putOrGet(verticesMap,segment.getDestination());
            origin.addNeighbor(destination, segment.getLength());
        }
    }

    /**
     * Constructor that build structure needed to computing shorter paths
     * @param map list of every intersections and segments
     * @param intersections useful intersections for witch we need to compute shorter-path-finding algorithm
     */
    public MapPathFinder(Map map, List<Intersection> intersections) {
        //System.out.println("MapPathFinder: <init> ");
        usefulIntersections = new HashSet<>();
        usefulIntersections.addAll(intersections);

        // We need to build whole adjacency lists from Map object
        verticesMap = new HashMap<>();

        for (Segment segment : map.getSegments()) {
            Vertex origin = putOrGet(verticesMap,segment.getOrigin());
            Vertex destination = putOrGet(verticesMap,segment.getDestination());
            origin.addNeighbor(destination, segment.getLength());
        }
    }

    /**
     * put or get a vertex linked to an {@link Intersection#id}
     * @param map Map from id to vertex
     * @param intersection id on a {@link Intersection}
     * @return the Vertex created or retrieved
     */
    private static Vertex putOrGet(java.util.Map<Long, Vertex> map, Intersection intersection){
        Vertex res = map.get(intersection.getId());
        if(res == null){
            res = new Vertex(intersection);
            map.put(intersection.getId(), res);
        }
        return res;
    }



    /**
     * find shorted paths from every useful point (depot, pickup or delivery)
     *
     * @return Best paths between useful intersections
     */
    public java.util.Map<Pair<Intersection, Intersection>, Path> findShorterPaths() {
        reducedGraph = new HashMap<>();

        java.util.Map<Integer, Vertex> intersectionIndexes = new HashMap<>();
        Vertex[] vertices = buildVerticesArray(usefulIntersections, intersectionIndexes);


        Dijkstra dijkstra = new Dijkstra(vertices);
        for (Integer originIndex : intersectionIndexes.keySet()) {
            dijkstra.start(originIndex);
            Vertex originVertex = intersectionIndexes.get(originIndex);

            for (int destinationIndex : intersectionIndexes.keySet()) {
                if (destinationIndex != originIndex) {
                    // We don't compute the distance between a vertex and itself
                    Path shorterPath = buildPath(destinationIndex, dijkstra.vertices);
                    Vertex destinationVertex = intersectionIndexes.get(destinationIndex);

                    reducedGraph.put(new Pair<>(originVertex.getIntersection(), destinationVertex.getIntersection()),
                            shorterPath);
                }
            }
        }
        return reducedGraph;
    }

    /**
     * Build a vertex Array and find useful vertex indices from useful intersection ids
     *
     * @param usefulIntersections [IN] useful intersections
     * @param intersectionIds   [OUT] array of indexes of the useful (related) vertices
     * @return Array of vertex
     */
    private Vertex[] buildVerticesArray(Set<Intersection> usefulIntersections,
                                        java.util.Map<Integer, Vertex>  intersectionIds) {
        int currentArrayIndex = 0;
        Vertex[] vertices = new Vertex[verticesMap.size()];

        for(Vertex vertex : verticesMap.values()){
            vertices[currentArrayIndex] = vertex;
            if(usefulIntersections.contains(vertex.getIntersection())){
                intersectionIds.put(currentArrayIndex, vertex);
            }
            currentArrayIndex++;
        }

        return vertices;
    }

    /**
     * Build a path from proceeded vertices and the index of the destination vertex
     *
     * @param indexEnd index of the destination vertex in the vertices' array
     * @param vertices array of proceeded vertices
     * @return Path
     */
    private Path buildPath(int indexEnd, Vertex[] vertices) {
        // Current vertex (start from the destination)
        Vertex current = vertices[indexEnd];
        Vertex predecessor;

        LinkedList<Intersection> pathList = new LinkedList<>();

        pathList.addFirst(current.getIntersection());
        while ((predecessor = current.getPredecessor()) != null) {
            current = predecessor;
            // add to the path the intersection corresponding to current vertex
            pathList.addFirst(current.getIntersection());
        }

        return new Path(pathList, vertices[indexEnd].getCurrentDistance());
    }
}
