package algorithm.tsp;


import algorithm.model.Graph;
import algorithm.model.Path;
import algorithm.spf.MapPathFinder;
import controller.Debug;
import javafx.util.Pair;
import model.Intersection;
import model.Map;
import model.PlanningRequest;
import model.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class allowing to simplify algorithm tasks
 *
 * Make execution of longer algorithm (TSP) on another thread
 * Contains also methods to add and remove requests
 */
public class FindPath extends Thread {
    /**
     * Tag used to debug
     */
    private static final String TAG = "FindPath";

    /**
     * Map from a pair of intersection <code>i1, i2</code> to the shorter path starting
     * at <code>i1</code> and ending at <code>i2</code>
     */
    private java.util.Map<Pair<Intersection, Intersection>, Path> shorterPath;

    /**
     * ordered list of useful intersections (pickups + deliveries + depot)
     */
    private List<Intersection> orderedListOfUsefulIntersections;

    private Map map;
    private PlanningRequest request;
    private FindPathObserver fpo;
    private TemplateTSP tsp;
    private int timeLimit = 20000;
    public static final float SPEED = (float) 15.0 / (float) (3.6 * 1000);

    /**
     * Constructor initializing variables needed
     *
     * @param map     Map of every intersections and segements
     * @param request Planning request to satisfy
     * @param fpo     Observer used to notify the end of the execution
     */
    public FindPath(Map map, PlanningRequest request, FindPathObserver fpo) {
        this.map = map;
        this.request = request;
        this.fpo = fpo;
    }

    /**
     * Method starting the Thread and the execution of {@link FindPath#findPath()}
     */
    @Override
    public void run() {
        fpo.endThread(findPath(), timeLimit);
    }

    /**
     * Set the time limit of the algorithm
     *
     * @param timeLimit new time limit
     */
    public void setTimeLimit(int timeLimit) {
        if (tsp != null) {
            tsp.setTimeLimit(timeLimit);
            this.timeLimit = timeLimit;
        }
    }

    /**
     * Calculate the path and return the corresponding state
     * The state contains planning request, arrival times, path...
     *
     * @return the corresponding state
     * @see algorithm.model.State
     */
    public algorithm.model.State findPath() {
        List<Intersection> path = find();
        List<Pair<Long, Intersection>> schedule = getSchedule(request);
        return new algorithm.model.State(path, schedule, orderedListOfUsefulIntersections, request);
    }

    /**
     * Method using TSP and SPF algorithm in order to find the best path matching the plannign request
     *
     * @return the best path found
     * @see MapPathFinder
     * @see TemplateTSP
     */
    public List<Intersection> find() {
        //Finds the time to go from any pickup/delivery/depot to another
        MapPathFinder pathFinder = new MapPathFinder(map, request);
        Debug.i(TAG, "MapPathFinder created");
        shorterPath = pathFinder.findShorterPaths();
        Debug.i(TAG, "Map created");

        //Search for a good path going through all previous points
        Graph g = new CompleteGraph(shorterPath, request);

        long startTime = System.currentTimeMillis();
        //TSP tsp = new TSP1();
        tsp = new TemplateTSP();
        Debug.i(TAG, "Search begin");
        tsp.searchSolution(timeLimit, g);

        //Printing/Returning response
        System.out.println("Solution of cost " + tsp.getSolutionCost() + " found in " + (System.currentTimeMillis() - startTime) + "ms");


        List<Intersection> requestsList = new ArrayList<>();
        orderedListOfUsefulIntersections = new ArrayList<>();
        for (int i = 0; i < g.getNbVertices() - 1; i++) {
            Intersection start = g.getIntersectionFromIndex(tsp.getSolution(i));
            orderedListOfUsefulIntersections.add(start);
            requestsList.addAll(shorterPath.get(
                    new Pair<>(start,
                            g.getIntersectionFromIndex(tsp.getSolution(i + 1)))).getVertices());
        }
        Intersection last = g.getIntersectionFromIndex(tsp.getSolution(g.getNbVertices() - 1));
        Intersection depot = g.getIntersectionFromIndex(tsp.getSolution(0));
        orderedListOfUsefulIntersections.add(last);
        orderedListOfUsefulIntersections.add(depot);
        requestsList.addAll(shorterPath.get(
                new Pair<>(last, depot)).getVertices());

        return requestsList;
    }

    /**
     * Get arrival times at pickups, deliveries and depot
     *
     * @param planning Planning request to satisfy
     * @return Ordered list of paired intersection and arrival time (as a Long in milliseconds)
     */
    public List<Pair<Long, Intersection>> getSchedule(PlanningRequest planning) {
        //HashMap<Intersection, Long> schedule = new HashMap<>();
        List<Pair<Long, Intersection>> schedule = new ArrayList<>();

        //HashMap which contains stop duration for each delivery and pickup
        HashMap<Intersection, Long> stopDurations = getStopDuration(planning);

        long departureTime = planning.getDepot().getDepartureTime().getTime();
        Intersection depart = null;
        float length;
        // speed en m/ms

        for (Intersection arrival : orderedListOfUsefulIntersections) {
            if (depart == null) {
                depart = arrival;
                continue;
            }
            Debug.i(TAG, "get Schedule foreach time = " + departureTime);
            /*
            try the find the path between the 2 intersections
            if the path exists, the 2 intersections are a delivery address and a pickup address, we want to know the arrival time
            if the path doesn't exit, one of the intersection or both are intermediaries intersection so we don't care about the arrival time
             */
            Path path = shorterPath.get(new Pair<>(depart, arrival));
            if (path != null) {
                length = path.getTotalLength();
                //add travel duration
                departureTime += timeForDistance(length);
                schedule.add(new Pair<>(departureTime, arrival));
                //add stop duration
                departureTime += stopDurations.get(arrival);
                //the next departure point is the current arrival point
                depart = arrival;
            }
        }
        return schedule;
    }

    /**
     * Read a {@link PlanningRequest} and find the duration of stops at deliveries and pickups
     *
     * @param planning planning request
     * @return HashMap linking the address to the stop duration (in milliseconds)
     */
    public static HashMap<Intersection, Long> getStopDuration(PlanningRequest planning) {
        HashMap<Intersection, Long> stopDuration = new HashMap<>();
        for (Request request : planning.getRequests()) {
            stopDuration.put(request.getDeliveryAddress(), (long) request.getDeliveryDuration() * 1000);
            stopDuration.put(request.getPickupAddress(), (long) request.getPickupDuration() * 1000);
        }
        stopDuration.put(planning.getDepot().getAddress(), (long) 0);
        return stopDuration;
    }

    /**
     * Add a request to the planning request
     * Try to find the better place where the request can be added without changing the arrival times at
     * the other useful intersections
     *
     * @param map          map, not modified
     * @param currentState current state, contains planning request, last computed path...
     * @param request      Delivery request to add
     * @return new State corresponding to the added request
     */
    public algorithm.model.State addRequest(final Map map, final algorithm.model.State currentState,
                                            Request request) {
        Debug.i(TAG, "add request (" + request + ")");
        //  1st, we need to compute path from (and to) new points to (and from) EVERY others
        //      a. Create the new planning request
        PlanningRequest newPlanningRequest = new PlanningRequest(currentState.planningRequest);
        newPlanningRequest.getRequests().add(request);

        //      b. Compute complete dijkstra (We need every from, so every multiple Dijkstra
        MapPathFinder pathFinder = new MapPathFinder(map, newPlanningRequest);
        Debug.i(TAG, "addRequest - start computing Dijkstra");
        long startTime;
        if (Debug.DEBUG) startTime = System.currentTimeMillis();
        shorterPath = pathFinder.findShorterPaths();
        if (Debug.DEBUG) {
            Debug.i(TAG, "addRequest - Dijkstra finished in " + (System.currentTimeMillis() - startTime) + "ms");
        }

        //  2nd, browse current path to find a place where we can insert the pickup
        //      a. Map arrival times
        java.util.Map<Intersection, Long> mappedArrivalTimes = new HashMap<>();
        for (Pair<Long, Intersection> arrivalTime : currentState.arrivalTimes) {
            mappedArrivalTimes.put(arrivalTime.getValue(), arrivalTime.getKey());
        }
        Debug.logMap(TAG, "addRequest - mappedArrivalTimes ", mappedArrivalTimes);

        java.util.Map<Intersection, Long> mappedPauseTimes = getStopDuration(newPlanningRequest);
        Debug.logMap(TAG, "addRequest - mappedPauseTimes ", mappedPauseTimes);

        //      b. Store data about the request added
        final Intersection pickupAddress = request.getPickupAddress();
        final Intersection deliveryAddress = request.getDeliveryAddress();
        final long pickupTime = mappedPauseTimes.get(request.getPickupAddress());
        final long deliveryTime = mappedPauseTimes.get(request.getDeliveryAddress());
        //      Also map those stop durations
        Debug.i(TAG,"addRequest : pickup time = "+pickupTime+" | delivery time = "+deliveryTime);

        //      c. search for an interval
        Intersection departure = null;     // The intersection visited before
        long departureTime = -1;        // The departure time from [before]
        boolean pickupPlaced = false;   // has the pickup been placed
        boolean deliveryPlaced = false; // has the delivery been placed
        List<Intersection> newOrderedUsefulIntersections = new ArrayList<>();
        List<Pair<Long, Intersection>> newArrivalTimes = new ArrayList<>(currentState.arrivalTimes);
        int newArrivalTimesIndex = 0;

        for (Intersection arrival : currentState.orderedListOfUsefulIntersections) {
            if (departure == null) { // At the beginning of the loop : store the depot as departure
                departure = arrival;
                newOrderedUsefulIntersections.add(departure);
                departureTime = newPlanningRequest.getDepot().getDepartureTime().getTime();
                continue;
            }

            if (!deliveryPlaced) {
                // Time when arrived to [arrival]
                long arrivalTime = mappedArrivalTimes.get(arrival);
                // Time from departure to arrival --> Travel time
                long travelTime = arrivalTime - departureTime;
                // Shorter time to travel, found with dijkstra
                long shorterTimeToTravel = timeForDistance(
                        shorterPath.get(new Pair<>(departure, arrival)).getTotalLength());

                if (travelTime > shorterTimeToTravel) {
                    // Actual travel time longer than the shorter possible
                    //      Indicates that a stop point was remove between [departure] and [arrival]
                    if (!pickupPlaced) {
                        // Build new possible path [departure -> pickup -> arrival]
                        Path departureToPickup = shorterPath.get(new Pair<>(departure, pickupAddress));
                        Path pickupToArrival = shorterPath.get(new Pair<>(pickupAddress, arrival));
                        long newTravelTime = timeForDistance(
                                departureToPickup.getTotalLength() + pickupToArrival.getTotalLength())
                                + pickupTime;
                        if (newTravelTime <= travelTime) {
                            // Pickup can be placed without changing the hours at [departure] and [arrival]
                            long timeAtPickup = departureTime + timeForDistance(departureToPickup.getTotalLength());
                            newOrderedUsefulIntersections.add(pickupAddress);
                            newArrivalTimes.add(newArrivalTimesIndex,
                                    new Pair<>(timeAtPickup, pickupAddress));
                            newArrivalTimesIndex++;

                            // Update values, in order to allow to add delivery between pickup and after, if it is possible
                            pickupPlaced = true;
                            departure = pickupAddress;
                            departureTime = timeAtPickup + pickupTime;
                            // Time that still available to make the trip to arrival :
                            travelTime = arrivalTime - departureTime;
                        }
                    }
                    if (pickupPlaced) {
                        Path departureToDelivery = shorterPath.get(new Pair<>(departure, deliveryAddress));
                        Path deliveryToArrival = shorterPath.get(new Pair<>(deliveryAddress, arrival));
                        long newTravelTime = timeForDistance(departureToDelivery.getTotalLength() +
                                deliveryToArrival.getTotalLength())
                                + deliveryTime;
                        if (newTravelTime <= travelTime) {
                            // Delivery can be placed without changing the hours at [departure] and [arrival]
                            long timeAtDelivery = departureTime + timeForDistance(departureToDelivery.getTotalLength());

                            newOrderedUsefulIntersections.add(deliveryAddress);
                            newArrivalTimes.add(newArrivalTimesIndex,
                                    new Pair<>(timeAtDelivery, deliveryAddress));
                            newArrivalTimesIndex++;
                            deliveryPlaced = true;
                        }
                    }
                }
            }
            departure = arrival;
            departureTime = mappedArrivalTimes.get(departure) + mappedPauseTimes.get(departure);
            newOrderedUsefulIntersections.add(departure);
            newArrivalTimesIndex++;
        }

        // If we couldn't place pickup and/or delivery into the current path, we add those at the end
        if (!deliveryPlaced) {
            if (!pickupPlaced) {
                // Add pickup just before final depot
                newOrderedUsefulIntersections.add(newOrderedUsefulIntersections.size() - 1, pickupAddress);

                // Retrieve penultimate point and arrival time
                Pair<Long, Intersection> penultimate = newArrivalTimes.get(newArrivalTimes.size() - 2);
                departureTime = penultimate.getKey() + mappedPauseTimes.get(penultimate.getValue());

                // Add arrival time at pickup
                Path penultimateToPickup = shorterPath.get(new Pair<>(penultimate.getValue(), pickupAddress));
                newArrivalTimes.add(newArrivalTimes.size() - 1,
                        new Pair<>(departureTime + timeForDistance(penultimateToPickup.getTotalLength()),
                                pickupAddress));
            }
            // Add delivery just before final depot
            newOrderedUsefulIntersections.add(newOrderedUsefulIntersections.size() - 1, deliveryAddress);

            // Retrieve penultimate values
            Pair<Long, Intersection> penultimate = newArrivalTimes.get(newArrivalTimes.size() - 2);
            departureTime = penultimate.getKey() + mappedPauseTimes.get(penultimate.getValue());

            // Add arrival time at delivery
            Path penultimateToDelivery = shorterPath.get(new Pair<>(penultimate.getValue(), deliveryAddress));
            final long timeAtDelivery = departureTime + timeForDistance(penultimateToDelivery.getTotalLength());
            newArrivalTimes.add(newArrivalTimes.size() - 1,
                    new Pair<>(timeAtDelivery, deliveryAddress));

            // We need to recalculate arrival time at depot
            final Intersection depot = orderedListOfUsefulIntersections.get(0);
            final Path deliveryToDepot = shorterPath.get(new Pair<>(deliveryAddress, depot));
            long finalArrivalTime =
                    timeAtDelivery + deliveryTime + timeForDistance(deliveryToDepot.getTotalLength());
            newArrivalTimes.set(newArrivalTimes.size() - 1, new Pair<>(finalArrivalTime, depot));
        }

        Debug.i(TAG, "addRequest - new list : " + Debug.toString(newOrderedUsefulIntersections, true));


        orderedListOfUsefulIntersections = newOrderedUsefulIntersections;

        Debug.i(TAG, "addRequest - arrival times : " + Debug.toString(newArrivalTimes, true));

        // Using orderListOfUsefulIntersections and shorterPath, build the new path matching the order
        final List<Intersection> newPath = buildPath();

        return new algorithm.model.State(
                newPath,
                newArrivalTimes,
                newOrderedUsefulIntersections,
                newPlanningRequest
        );
    }

    /**
     * Remove a delivery request from the planning request
     * And update the paths without changing arrival times
     *
     * @param currentState state before removing of the request, unchanged
     * @param toRemove     request to remove
     * @return State after removing the request
     */
    public algorithm.model.State removeRequest(final algorithm.model.State currentState,
                                               final Request toRemove) {
        // Retrieve values
        final Intersection pickup = toRemove.getPickupAddress();
        final Intersection delivery = toRemove.getDeliveryAddress();
        List<Intersection> newOrderedListOfIntersections = new ArrayList<>(currentState.orderedListOfUsefulIntersections);

        // Remove pickup and delivery from ordered list
        newOrderedListOfIntersections.remove(pickup);
        newOrderedListOfIntersections.remove(delivery);

        // Remove arrival times about request
        List<Pair<Long, Intersection>> newArrivalTimes = new ArrayList<>(currentState.arrivalTimes);
        for (int i = 0; i < newArrivalTimes.size(); i++) {
            if (newArrivalTimes.get(i).getValue().equals(pickup)) {
                newArrivalTimes.remove(i);
                i--;
            } else if (newArrivalTimes.get(i).getValue().equals(delivery)) {
                newArrivalTimes.remove(i);
                i--;
            }
        }

        // Re-build path
        orderedListOfUsefulIntersections = newOrderedListOfIntersections;
        List<Intersection> newPath = buildPath();

        // Re-build planning request
        PlanningRequest newPlanningRequest = new PlanningRequest(currentState.planningRequest);
        newPlanningRequest.getRequests().remove(toRemove);

        // Re-find hour at depot (the only the can change)
        final Intersection depot = newOrderedListOfIntersections.get(0);
        final Intersection penultimate = newOrderedListOfIntersections.get(newOrderedListOfIntersections.size() - 2);
        //  departure time from penultimate
        final long departureTime = newArrivalTimes.get(newArrivalTimes.size() - 2).getKey() +
                getStopDuration(newPlanningRequest).get(penultimate);
        final Path penultimateToDepot = shorterPath.get(new Pair<>(penultimate, depot));
        long finalArrivalTime = departureTime + timeForDistance(penultimateToDepot.getTotalLength());
        newArrivalTimes.set(newArrivalTimes.size() - 1, new Pair<>(finalArrivalTime, depot));

        /*
        // Remove pickup
        List<Intersection> newPath = removePoint(newOrderedListOfIntersections,
                currentState.path, pickup);
        newOrderedListOfIntersections.remove(pickup);

        // Remove delivery
        newPath = removePoint(newOrderedListOfIntersections,
                newPath, delivery);
        newOrderedListOfIntersections.remove(delivery);
        */

        return new algorithm.model.State(
                newPath,
                newArrivalTimes,
                newOrderedListOfIntersections,
                newPlanningRequest);
    }

    /**
     * Using {@link FindPath#orderedListOfUsefulIntersections} and {@link FindPath#shorterPath} build
     * the best path linking the useful intersection in the rigth order
     * @return best path founded
     */
    private List<Intersection> buildPath() {
        List<Intersection> path = new ArrayList<>();
        Intersection before = null;
        for (Intersection after : orderedListOfUsefulIntersections) {
            if (before == null) {
                before = after;
                continue;
            }
            path.addAll(shorterPath.get(new Pair<>(before, after)).getVertices());
            before = after;
        }
        return path;
    }

    /**
     * @param distance Distance in meters
     * @return the time needed to travel the <code>distance</code> in milliseconds
     * @see FindPath#SPEED
     */
    public static long timeForDistance(float distance) {
        return (long) (distance / SPEED);
    }

    /**
     * Interface used to notify the end of the Thread
     */
    public interface FindPathObserver {
        /**
         * Method called a the end of the calculation
         * @param newState state containing new information
         * @param timeLimit time limit (0 if canceled)
         */
        void endThread(algorithm.model.State newState, int timeLimit);
    }
}
