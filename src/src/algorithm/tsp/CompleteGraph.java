package algorithm.tsp;

import algorithm.model.Graph;
import algorithm.model.Path;
import javafx.util.Pair;
import model.Intersection;
import model.PlanningRequest;
import model.Request;

import java.util.HashMap;
import java.util.Map;

public class CompleteGraph implements Graph {
    /**
     * Number of vertices
     */
    protected int nbVertices;

    /**
     * Array of intersection, with [0] the depot
     */
    protected Intersection[] intersections;

    /**
     * Map of a Pair of Intersection, Path, representing the path between each pair of intersections
     */
    protected Map<Pair<Intersection, Intersection>, Path> map;
    /**
     * Map of Integer,Integer representing precedences
     */
    protected Map<Integer, Integer> precedence;
    /**
     * 2D tab representing the matrice of the graph
     */
    protected float[][] matrice;

    /**
     * Create a complete directed graph such that each edge has a weight within [MIN_COST,MAX_COST]
     *
     * @param map the map to turn into graph
     * @param planningRequest the planning request
     */
    public CompleteGraph(Map<Pair<Intersection, Intersection>, Path> map, PlanningRequest planningRequest) {
        this.map = map;

        nbVertices = 1+planningRequest.getRequests().size()*2;

        intersections = new Intersection[nbVertices];
        intersections[0] = planningRequest.getDepot().getAddress();


        precedence = new HashMap<>();

        int currentIndex = 1;
        for (Request request : planningRequest.getRequests()) {
            int pickupIndex = currentIndex;
            int deliveryIndex = currentIndex +1;
            // Pickup
            intersections[pickupIndex] = request.getPickupAddress();

            // Delivery
            intersections[deliveryIndex] = request.getDeliveryAddress();

            // Precedence
            precedence.put(pickupIndex,deliveryIndex);

            currentIndex+=2;
        }

        // On crée la matrice d'ajacence du graph
        matrice = createMatrice();
    }

    /**
     *Returns a 2D tab representing the matrice of the graph
     *
     * @return a 2D tab representing the matrice of the graph
     */
    public float[][] createMatrice(){
        int nbVertices = intersections.length;
        float[][] matrice = new float[nbVertices][nbVertices];
        for(int i = 0; i < nbVertices; i++){
            for(int j = 0; j < nbVertices; j++){
                if(i == j)
                    matrice[i][j] = -1;
                else
                    matrice[i][j] = map.get(new Pair(intersections[i],intersections[j])).getTotalLength();
            }
        }
        return matrice;
    }

    /**
     *Returns the matrice of the
     *
     * @return a HashMap containing precedence
     */
    @Override
    public float[][] getMatrice(){
       return matrice;
    }

    /**
     *Returns the map of the precedences
     *
     * @return a HashMap containing precedence
     */
    @Override
    public Map<Integer, Integer> getPrecedence() {
        return precedence;
    }

    /**
     *Returns the intersections from index i
     *
     * @param i the intersection wanted
     * @return a tab containing the intersections from index i
     */
    @Override
    public Intersection getIntersectionFromIndex(int i) {
        return intersections[i];
    }

    /**
     *Returns the number of vertices of the graph
     *
     * @return the number of vertices of the graph
     */
    @Override
    public int getNbVertices() {
        return nbVertices;
    }

    /**
     * Returns the cost of the arc between i and j
     *
     * @param i integer representing the departure point
     * @param j integer representing the destination point
     * @return the cost of the arc
     */
    @Override
    public float getCost(int i, int j) {
        if (!isArc(i,j))
            return -1;
        return map.get(new Pair<>(intersections[i], intersections[j])).getTotalLength();
    }
    /**
     * Returns true if there is an arc between i and j
     *
     * @param i integer representing the departure point
     * @param j integer representing the destination point
     * @return true if there is an arc, false else
     */
    @Override
    public boolean isArc(int i, int j) {
        if (i < 0 || i >= nbVertices || j < 0 || j >= nbVertices)
            return false;
        return i != j;
    }

}
