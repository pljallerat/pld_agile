package algorithm.tsp;

import algorithm.model.Graph;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Class used to solve TSP (Travelling salesman problem)
 */
public class TemplateTSP {
    private Integer[] bestSol;
    protected Graph graph;
    private float bestSolCost;
    private int timeLimit;
    private long startTime;

    /**
     * Search for a shortest cost hamiltonian circuit in <code>graph</code> within <code>timeLimit</code> milliseconds
     * (returns the best found tour whenever the time limit is reached)
     * Warning: The computed tour always start from vertex 0
     *
     * @param timeLimit
     * @param graph
     */
    public void searchSolution(int timeLimit, Graph graph) {
        if (timeLimit <= 0) return;
        startTime = System.currentTimeMillis();
        this.timeLimit = timeLimit;
        this.graph = graph;

        bestSol = new Integer[graph.getNbVertices()];

        Collection<Integer> unvisited = new ArrayList<>(graph.getNbVertices() - 1);

        unvisited.addAll(graph.getPrecedence().keySet());

        Collection<Integer> visited = new ArrayList<>(graph.getNbVertices());
        visited.add(0); // On visite le dépot

        bestSolCost = Integer.MAX_VALUE;
        branchAndBound(0, unvisited, visited, 0, graph.getMatrice(),0);
    }

    /**
     * @param i
     * @return the ith visited vertex in the solution computed by <code>searchSolution</code>
     * (-1 if <code>searcheSolution</code> has not been called yet, or if i < 0 or i >= graph.getNbSommets())
     */
    public Integer getSolution(int i) {
        if (graph != null && i >= 0 && i < graph.getNbVertices())
            return bestSol[i];
        return -1;
    }

    /**
     * @return the total cost of the solution computed by <code>searchSolution</code>
     * (-1 if <code>searcheSolution</code> has not been called yet).
     */
    public float getSolutionCost() {
        if (graph != null)
            return bestSolCost;
        return -1;
    }

    public void setTimeLimit(int timeLimit){
        this.timeLimit = timeLimit;
    }

    /**
     * Template method of a branch and bound algorithm for solving the TSP in <code>graph</code>.
     *
     * @param currentVertex the last visited vertex
     * @param unvisited     the set of vertex that have not yet been visited
     * @param visited       the sequence of vertices that have been already visited (including currentVertex)
     * @param currentCost   the cost of the path corresponding to <code>visited</code>
     */
    private void branchAndBound(int currentVertex, Collection<Integer> unvisited,
                                Collection<Integer> visited, float currentCost, float[][] matrice, int precedentVertex) {
        if (System.currentTimeMillis() - startTime > timeLimit) return;

        if (unvisited.size() == 0) {
            // Every vertices have been visited : return to depot
            if (graph.isArc(currentVertex, 0)) {
                if (currentCost < bestSolCost) {
                    // A better solution has been found : update the values
                    visited.toArray(bestSol);
                    bestSolCost = currentCost;
                }
            }

        } else if (currentCost < bestSolCost) {
            // There is a new better solution
            Iterator<Pair<Pair<Float,Float[][]>,Integer>> it = new SeqIter(unvisited, currentVertex, graph, matrice, currentCost);
            while (it.hasNext()) {
                Pair<Pair<Float,Float[][]>,Integer> next = it.next();
                int nextVertex = next.getValue();
                // Update visited ans unvisited lists
                visited.add(nextVertex);
                unvisited.remove(nextVertex);
                // If we visit a pickup, we add the delivery has a point to visit
                Integer pointToGo;
                if ((pointToGo = graph.getPrecedence().get(nextVertex)) != null)
                    unvisited.add(pointToGo); //(Update : call only once the Map.get (it's a complex method, in log(n))

                // Keep on searching the best solution
                branchAndBound(nextVertex, unvisited, visited,
                        next.getKey().getKey(), float[][].class.cast(next.getKey().getValue()), currentVertex);

                // Cancel the update
                visited.remove(nextVertex);
                if ((pointToGo = graph.getPrecedence().get(nextVertex)) != null)
                    unvisited.remove(pointToGo);
                unvisited.add(nextVertex);
            }
        }
    }
}
