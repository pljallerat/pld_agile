package algorithm.tsp;

import algorithm.model.Graph;
import javafx.util.Pair;

import java.util.*;

/**
 * Iterator used by {@link TemplateTSP}
 * an iterator for visiting all vertices in <code>unvisited</code> which are successors of <code>currentVertex</code> with their costs and their adjacences matrices
 */
public class SeqIter implements Iterator<Pair<Pair<Float, Float[][]>, Integer>> {
    private final List<Pair<Pair<Float, Float[][]>, Integer>> candidates;

    /**
     * Create an iterator to traverse the set of vertices in <code>unvisited</code>
     * which are successors of <code>currentVertex</code> in <code>graph</code>
     * Vertices are traversed in the increasing cost order
     *
     * @param unvisited     unvisited vertices' indexes
     * @param currentVertex current vertex
     * @param g             whole graph
     * @param matrice       Current adjacency matrix
     * @param actualCost    current cost
     */
    public SeqIter(Collection<Integer> unvisited, int currentVertex, Graph g, float[][] matrice, float actualCost) {
        candidates = new ArrayList<>();
        for (Integer s : unvisited)
            if (g.isArc(currentVertex, s))
                candidates.add(new Pair<>(bound(s, currentVertex, g.getNbVertices(), matrice, actualCost), s));
        candidates.sort((p1, p2) -> (int) (p1.getKey().getKey() - p2.getKey().getKey()));
    }

    /**
     * @return if the iterator still contains candidates to visit
     */
    @Override
    public boolean hasNext() {
        return candidates.size() > 0;
    }

    /**
     * @return the next candidate
     */
    @Override
    public Pair<Pair<Float, Float[][]>, Integer> next() {
        Pair<Pair<Float, Float[][]>, Integer> i = candidates.get(0);
        candidates.remove(0);
        return i;
    }

    @Override
    public void remove() {
    }

    /**
     * Method bound used by the algorithm
     *
     * @param currentVertex   current vertex
     * @param precedentVertex vertex before
     * @param NbVertices      number of vertices
     * @param m               matrix
     * @param precedentCost   precedent cost
     * @return the minimal cost and the corresponding matrix
     */
    public static Pair<Float, Float[][]> bound(Integer currentVertex,
                                               Integer precedentVertex, int NbVertices,
                                               float[][] m, float precedentCost) {
        /*
        Source/Explication de l'heuristique
        https://www.youtube.com/watch?v=1FEP_sNb62k
         */
        float[][] matrice = new float[m.length][m.length];
        for (int i = 0; i < m.length; i++)
            System.arraycopy(m[i], 0, matrice[i], 0, m.length);
        if (currentVertex.equals(precedentVertex))
            return reduceMatrice(matrice, NbVertices);

        float cost = matrice[precedentVertex][currentVertex];
        for (int i = 0; i < NbVertices; i++)
            matrice[i][currentVertex] = -1;
        for (int j = 0; j < NbVertices; j++)
            matrice[precedentVertex][j] = -1;
        matrice[currentVertex][precedentVertex] = -1;

        Pair<Float, Float[][]> mat = reduceMatrice(matrice, NbVertices);
        cost += precedentCost + mat.getKey();
        return new Pair<>(cost, mat.getValue());
    }

    /**
     * Reduces the matrix given without modifying it
     *
     * @param m the matrix to reduce
     * @return returns the minimal cost and the corresponding matrix
     */
    public static Pair<Float, Float[][]> reduceMatrice(float[][] m, int NbVertices) {
        float cost = 0;
        for (int i = 0; i < NbVertices; i++) {
            float min = Float.MAX_VALUE;
            for (int j = 0; j < NbVertices; j++) {
                if (m[i][j] < min && m[i][j] != -1) {
                    min = m[i][j];
                }
            }
            if (min != Float.MAX_VALUE && min > 0) {
                cost += min;
                for (int j = 0; j < NbVertices; j++) {
                    if (m[i][j] != -1)
                        m[i][j] -= min;
                }
            }
        }
        for (int j = 0; j < NbVertices; j++) {
            float min = Float.MAX_VALUE;
            for (int i = 0; i < NbVertices; i++) {
                if (m[i][j] < min && m[i][j] != -1)
                    min = m[i][j];
            }
            if (min != Float.MAX_VALUE && min > 0) {
                cost += min;
                for (int i = 0; i < NbVertices; i++) {
                    if (m[i][j] != -1)
                        m[i][j] -= min;
                }
            }
        }
        return new Pair(cost, m);
    }
}
