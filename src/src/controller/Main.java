package controller;

/**
 * Launcher of the app
 */
public class Main {

    /**
     * Main methods, starting the apps
     * @param args command line options, unused
     */
    public static void main(String[] args){
        new Controller();
    }
}
