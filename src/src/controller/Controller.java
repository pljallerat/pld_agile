package controller;

import XML_processing.MapProcessing;
import XML_processing.RequestProcessing;
import algorithm.model.State;
import algorithm.model.StateHistory;
import algorithm.tsp.FindPath;
import ihm.Frame;
import model.Map;
import model.PlanningRequest;
import model.Request;

/**
 * Class <code>controller</code> in MVC pattern, used to link the <code>model</code> and the <code>view</code>
 */
public class Controller implements ViewObserver, FindPath.FindPathObserver {
    private static final String TAG = "Controller";

    // View
    Frame frame;

    // Model
    Map map;
    StateHistory stateHistory = new StateHistory();
    private FindPath fp;

    /**
     * Constructor
     */
    public Controller() {
        Debug.i(TAG, "<init>");
        frame = new Frame(this);
    }

    /**
     * Ask from the view to load a Map
     * @param filePath path of the file to load as a {@link Map}
     */
    @Override
    public void loadMap(String filePath) {
        Debug.i(TAG, "load Map () {filePath='" + filePath + "'}");
        try {
            map = MapProcessing.manageMap(filePath);
            // The map is well loaded
            frame.drawMap(MapProcessing.getSegmentSet(), MapProcessing.getMapOffset());
            frame.drawPoints(null);
        } catch (Exception e) {
            Debug.i(TAG, "load Map Error : " + e.getMessage(), e.getCause());
            frame.showError("Erreur lors du chargement de la carte : " + e.getMessage());
        }

    }

    /**
     * Ask from the view to load a Planning Request
     * @param filePath path of the file to load as a {@link PlanningRequest}
     */
    @Override
    public void loadRequest(String filePath) {
        Debug.i(TAG, "load Request () {filePath='" + filePath + "'}");

        try {
            PlanningRequest planningRequest = RequestProcessing.generateRequestList(filePath);
            State state = new State(null, null, null, planningRequest);
            stateHistory.clearHistory();
            stateHistory.addModification(state);
            drawState(state);
        } catch (Exception e) {
            Debug.i(TAG, "load Request Error : " + e.getMessage(), e.getCause());
            frame.showError("Erreur lors du chargement des requêtes: " + e.getMessage());
        }
    }

    /**
     * Ask from the view to find the best path matching the planning request
     */
    @Override
    public void executeRequest() {
        Debug.i(TAG, "executeRequest()");
        fp = new FindPath(map, stateHistory.getCurrentState().planningRequest, this);
        fp.start();
    }

    /**
     * Ask from the view to stop the execution of the thread
     */
    @Override
    public void stopThread() {
        System.out.println("prise en compte");
        if (fp != null) {
            fp.setTimeLimit(0);
        }
        frame.drawPath(null);
    }

    /**
     * Method called at the end of the execution of TSP
     * @see FindPath
     *
     * @param newState  New state
     * @param timeLimit Time limit (if 0, then the execution was canceled and we don't show the path)
     */
    @Override
    public void endThread(State newState, int timeLimit) {
        if (timeLimit == 0)
            return;
        Debug.i(TAG, "executeRequest() - Path found");
        stateHistory.addModification(newState);

        //List<Pair<Long, Intersection>> arrivalTimes = fp.getSchedule(stateHistory.getCurrentState().planningRequest);
        //Debug.i(TAG, "executeRequest() - arrival times found");
        //frame.setArrivalTimes(arrivalTimes);
        drawState(newState);
    }

    /**
     * Ask from the view to add a new delivery request
     * @param newRequest delivery request to add
     */
    @Override
    public void addRequest(Request newRequest) {
        Debug.i(TAG, "add request : " + newRequest.getPickupAddress() + " " + newRequest.getDeliveryAddress());
        // First version : add a request at the end of the path

        State newState = fp.addRequest(map, stateHistory.getCurrentState(), newRequest);
        stateHistory.addModification(newState);
        drawState(newState);
    }

    /**
     * Ask from the view to remove a delivery request
     * @param request delivery request to remove
     */
    @Override
    public void removeRequest(Request request) {
        Debug.i(TAG, "remove request : " + request);
        // Remove a request

        if(stateHistory.getCurrentState().planningRequest.getRequests().size() <= 1){
            frame.showError("Vous ne pouvez pas suprimer la dernière requête !");
            return;
        }

        State newState = fp.removeRequest(stateHistory.getCurrentState(), request);
        stateHistory.addModification(newState);
        drawState(newState);
    }

    /**
     * Undo last change
     */
    @Override
    public void undo() {
        stateHistory.undo();
        drawState(stateHistory.getCurrentState());
    }

    /**
     * Redo last undone change
     */
    @Override
    public void redo() {
        stateHistory.redo();
        drawState(stateHistory.getCurrentState());
    }

    /**
     * Show a state
     * @param state State of the application
     */
    public void drawState(State state) {
        Debug.i(TAG, "draw state");
        frame.drawPoints(state.planningRequest);
        if (state.path != null) {
            frame.drawPath(state.path);
        }
        frame.setArrivalTimes(state.arrivalTimes);
    }
}
