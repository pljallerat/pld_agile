package controller;

import model.Request;

/**
 * Interface used to observer the view
 */
public interface ViewObserver {
    /**
     * Ask to load a {@link model.Map}
     * @param filePath path to map file
     */
    void loadMap(String filePath);

    /**
     * Ask to load a {@link model.PlanningRequest}
     * @param filePath path to Planning request file
     */
    void loadRequest(String filePath);

    /**
     * Ask to find best path satisfying planning request
     */
    void executeRequest();

    /**
     * Stop the execution of the thread
     */
    void stopThread();

    /**
     * Add a request to the path
     * @param r request to add
     */
    void addRequest(Request r);

    /**
     * Remove a request to the path
     * @param request request to remove
     */
    void removeRequest(Request request);

    /**
     * Undo last change
     */
    void undo();

    /**
     * Redo last undone change
     */
    void redo();
}
