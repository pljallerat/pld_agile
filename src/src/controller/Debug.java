package controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Class used to debug
 */
public class Debug {
    /**
     * Are we in debug mode
     */
    public static final boolean DEBUG = true;

    /**
     * Do we Print the time when logging information
     */
    private static final boolean LOG_TIME = true;

    /**
     * Formatter used to show log time
     */
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /**
     * Log information
     * @param tag Name tag of the caller
     * @param message Message to show
     */
    public static void i(String tag, String message) {
        if (DEBUG) {
            System.out.println(buildMessage(tag, message));
        }
    }

    /**
     * Log information and stack trace
     * @param tag Name tag of the caller
     * @param message Message to show
     * @param throwable stack trace
     */
    public static void i(String tag, String message, Throwable throwable) {
        if (DEBUG) {
            System.out.println(buildMessage(tag, message));
            System.out.println(throwable);
        }
    }


    /**
     * Build a loggable message
     * @param tag Tag of the caller
     * @param message message to log
     * @return a string message containing relevant information
     */
    private static String buildMessage(String tag, String message) {
        return (LOG_TIME ? dateFormat.format(System.currentTimeMillis()) + " | " : "") + tag + ": " + message;
    }

    /**
     * Log a map content
     * @param tag Name tag of the caller
     * @param baseMessage message to print
     * @param map map to print
     */
    public static void logMap(String tag, String baseMessage, Map map) {
        if (DEBUG) {
            StringBuilder builder = new StringBuilder(baseMessage + "  :  {\n");
            for (Object key : map.keySet()) {
                builder.append(key).append(" : ").append(map.get(key)).append("\n");
            }
            builder.append("}");
            System.out.println(buildMessage(tag, builder.toString()));
        }
    }

    /**
     * Method building a String describing a list
     * @param list list to stringify
     * @param newLines does we apply new lines between elements
     * @return string describing the list
     */
    public static String toString(List list, boolean newLines) {
        StringBuilder builder = new StringBuilder("{");
        for (Object val : list) {
            builder.append(val).append(", ");
            if (newLines)
                builder.append("\n");
        }
        return builder.append("}").toString();
    }
}
