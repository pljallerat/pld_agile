package ihm;

import controller.Debug;
import javafx.util.Pair;
import model.Intersection;
import model.PlanningRequest;
import model.Request;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Map;

/**
 * RightPanel is used to show the table of all useful intersection (depot + pickups + deliveruies)
 * It also shows arrival times to those, when calculated
 *
 */
public class RightPanel extends JPanel {

    private static final String TAG = "RightPanel";

    /**
     * JTable used to show information
     */
    private TimeTable timeTable;

    private JButton undoButton, redoButton;
    private JLabel jl1;

    private JPanel buttonpan;
    private JButton forward;
    private JButton backward;

    /**
     * Constructor of RightPanel
     * @param observer RightPanelObserver allow communication between the BottomLeftPanel and the Frame
     */
    public RightPanel(RightPanelObserver observer) {
        super();
        Debug.i(TAG, "<init>");

        //Right panel
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.black));

        GridBagConstraints gcon = new GridBagConstraints();

        gcon.weightx = 0.1;
        gcon.weighty = 0.1;

        gcon.gridx = 0;
        gcon.gridy = 0;
        gcon.gridwidth = 1;
        gcon.gridheight = 1;
        gcon.anchor = GridBagConstraints.CENTER;


        //icon button forward
        Icon icon = new ImageIcon("images/right.PNG");
        //forward button
        forward = new JButton(icon);
        forward.setText(" Suivant");
        forward.setHorizontalTextPosition(SwingConstants.LEFT);
        forward.setEnabled(true);
        //Button forward action
        forward.addActionListener(e -> {
            if(timeTable.getRowCount() > 0) {
                int rowNb = timeTable.getSelectedRow();
                if (rowNb == -1 || rowNb == timeTable.getRowCount() - 1) {
                    timeTable.setRowSelectionInterval(0, 0);
                } else {
                    timeTable.setRowSelectionInterval(rowNb + 1, rowNb + 1);
                }
                timeTable.highlightIntersection();
            }
        });


        //icon button backward
        Icon icon2 = new ImageIcon("images/left.PNG");
        backward = new JButton(icon2);
        backward.setText("Precedent");
        backward.setEnabled(true);
        //Button backward action
        backward.addActionListener(e -> {
            if(timeTable.getRowCount() > 0) {
                int rowNb = timeTable.getSelectedRow();
                if (rowNb == -1 || rowNb == 0) {
                    timeTable.setRowSelectionInterval(timeTable.getRowCount() - 1, timeTable.getRowCount() - 1);
                } else {
                    timeTable.setRowSelectionInterval(rowNb - 1, rowNb - 1);
                }
                timeTable.highlightIntersection();
            }
        });
        gbl.setConstraints(backward,gcon);
        add(backward);
        gcon.gridx = 4;
        gbl.setConstraints(forward,gcon);
        add(forward);

        gcon.gridx = 0;
        gcon.gridy = 1;
        gcon.gridheight =  2;
        gcon.gridwidth = 13;
        timeTable = new TimeTable(observer);
        add(new JScrollPane(timeTable), gcon);

        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.black));


        undoButton = new JButton("Annuler");
        undoButton.addActionListener( e -> {
            observer.undo();
            observer.changeNumberOfModifications(-1);
            warn(observer);
        });
        undoButton.setEnabled(false);
        gcon.gridx = 0;
        gcon.gridy = 7;
        gcon.gridwidth = 4;
        gcon.gridheight = 1;
        gbl.setConstraints(undoButton,gcon);
        add(undoButton);

        redoButton = new JButton("Refaire");
        redoButton.addActionListener( e -> {
            observer.redo();
            observer.changeNumberOfModifications(1);
            warn(observer);
        });
        redoButton.setEnabled(false);
        gcon.gridx = 4;
        gbl.setConstraints(redoButton,gcon);
        add(redoButton);
    }

    /**
     * When a request modification is done (add or delete a point), it updates the buttons displays.
     * They become enable or disable
     * @param observer Object containing useful information
     */
    public void warn(RightPanelObserver observer) {
        undoButton.setEnabled(observer.getnbOfModifications() > 0);
        redoButton.setEnabled(observer.getTotalNbOfChanges() - observer.getnbOfModifications() > 0);
    }

    /**
     * Set the planning request : populate the table
     */
    public void setPlanningRequest(PlanningRequest planningRequest) {
        Debug.i(TAG, "set Planning Request");
        timeTable.setAddresses(planningRequest);
    }

    /**
     * set arrival times
     * List of intersections and arrival time
     *
     * @param arrivalTimes list of intersections and arrival times
     * @see algorithm.tsp.FindPath#getSchedule(PlanningRequest)
     */
    public void setArrivalTimes(List<Pair<Long, Intersection>> arrivalTimes) {
        timeTable.setArrivalTimes(arrivalTimes);
    }


    /**
     * Select an intersection in the table
     * if the intersection isn't in the table, doesn't do anything
     */
    public void selectIntersection(Intersection intersection) {
        timeTable.selectIntersection(intersection);
    }

    /**
     * class TimeTable, instance of JTable
     * show information of useful intersections
     */
    private static class TimeTable extends JTable implements MouseListener {
        private ihm.RightPanel.RightPanelObserver observer;
        private static final String TAG = RightPanel.TAG + ".TimeTable";
        private static final String[] COLUMN_NAMES = {
                "Ordre de passage",
                "Type",
                "Heure d'arrivée"};
        private DateFormat formatter;
        private Map<Integer, Intersection> indexToIntersection;
        private Map<Intersection, Integer> intersectionsLineIndices;
        private IntersectionTypes intersectionTypes;
        DefaultTableModel model;

        /**
         * Column TimeTable
         *  @param observer RightPanelObserver allow communication between the BottomLeftPanel and the Frame
         */
        public TimeTable(RightPanelObserver observer) {
            super();
            addMouseListener(this);
            this.observer = observer;
            model = new DefaultTableModel(COLUMN_NAMES, 0){
                public boolean isCellEditable(int row, int column) {
                    return false;
                }

            };

            //Sets the data model for this table to newModel and registers with it for listener notifications from the new data model.
            setModel(model);
            formatter = new SimpleDateFormat("HH:mm", Locale.FRENCH);
            setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        }

        public void setAddresses(PlanningRequest planningRequest) {
            Debug.i(TAG, "setAddresses");
            if(planningRequest == null){
                model.setNumRows(0);
                return;
            }

            intersectionsLineIndices = new HashMap<>();
            indexToIntersection = new HashMap<>();
            intersectionTypes = new IntersectionTypes(planningRequest);

            model.setNumRows(0);
            //Adds a row to the end of the model
            model.addRow(new String[]{
                    "0",
                    IntersectionTypes.DEPOT,
                    formatter.format(planningRequest.getDepot().getDepartureTime())
            });
            indexToIntersection.put(0, planningRequest.getDepot().getAddress());
            intersectionsLineIndices.put(planningRequest.getDepot().getAddress(), 0);
            int current = 1;
            for (Request request : planningRequest.getRequests()) {
                model.addRow(new String[]{
                        "" + current,
                        IntersectionTypes.PICKUP,
                        "?"
                });
                indexToIntersection.put(current, request.getPickupAddress());
                intersectionsLineIndices.put(request.getPickupAddress(), current);
                current++;
                model.addRow(new String[]{
                        "" + current,
                        IntersectionTypes.DELIVERY,
                        "?"
                });
                indexToIntersection.put(current, request.getDeliveryAddress());
                intersectionsLineIndices.put(request.getDeliveryAddress(), current);
                current++;
            }
            model.addRow(new String[]{
                    ""+current,
                    IntersectionTypes.DEPOT,
                    "?"
            });
            indexToIntersection.put(current, planningRequest.getDepot().getAddress());
            intersectionsLineIndices.put(planningRequest.getDepot().getAddress(), current);



        }

        public void setArrivalTimes(List<Pair<Long, Intersection>> arrivalTimes) {
            if(arrivalTimes == null){
                return;
            }
            int rowIndex = 1;

            model.setNumRows(1);

            for (Pair<Long, Intersection> pair : arrivalTimes) {
                //Debug.i("RightPanel", "add pair " + pair + " -- " + rowIndex);
                intersectionsLineIndices.put(pair.getValue(), rowIndex);
                indexToIntersection.put(rowIndex, pair.getValue());
                model.addRow(new String[]{
                        "" + rowIndex,
                        intersectionTypes.getType(pair.getValue()),
                        formatter.format(pair.getKey()),
                        pair.getValue().getId().toString()
                });
                rowIndex++;
            }
        }


        public void selectIntersection(Intersection intersection) {
            try {
                final Integer rowIndex = intersectionsLineIndices.get(intersection);
                if (rowIndex != null) {
                    Debug.i(TAG, "actually selection intersection " + intersection);

                    setRowSelectionInterval(rowIndex, rowIndex);
                }
            }catch (Exception e){}
        }

        public void highlightIntersection() {
            observer.highlightIntersectionInMap(indexToIntersection.get(getSelectedRow()));
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            int row = this.rowAtPoint(e.getPoint());
            String id = getValueAt(row,0).toString();
            observer.highlightIntersectionInMap(indexToIntersection.get(Integer.parseInt(id)));

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    /**
     * inner class used to know types of intersections
     */
    private static class IntersectionTypes {
        private Set<Intersection> pickups;
        private Set<Intersection> deliveries;
        private Intersection depot;
        public static final String DEPOT = "Depot", DELIVERY = "Delivery", PICKUP = "Pickup";

        /**
         * Constructor from a {@link PlanningRequest}
         * @param planningRequest request
         */
        public IntersectionTypes(PlanningRequest planningRequest) {
            depot = planningRequest.getDepot().getAddress();

            pickups = new HashSet<>();
            deliveries = new HashSet<>();

            for (Request request : planningRequest.getRequests()) {
                pickups.add(request.getPickupAddress());
                deliveries.add(request.getDeliveryAddress());
            }

            Debug.i("TAG","p : "+Arrays.toString(pickups.toArray(new Intersection[]{}))+
            "   d "+Arrays.toString(deliveries.toArray(new Intersection[]{})));
        }

        /**
         * @param intersection intersection to search type of
         * @return type of this intersection
         */
        public String getType(Intersection intersection) {
            String res = "";
            if (depot.equals(intersection)) {
                res = DEPOT;
            }
            if (pickups.contains(intersection)) {
                res = res.isEmpty() ? PICKUP : res + " / " + PICKUP;
            }
            if (deliveries.contains(intersection)) {
                res = res.isEmpty() ? DELIVERY : res + " / " + DELIVERY;
            }
            return res.isEmpty() ? "???" : res;
        }
    }

    /**
     * Interface used to observe this panel
     */
    public interface RightPanelObserver {
        /**
         * Highlight an intersection in the map view
         * @param i intersection to highlight
         */
        void highlightIntersectionInMap(Intersection i);

        /**
         * Undo last modification
         */
        void undo();

        /**
         * Redo last done modification
         */
        void redo();

        /**
         * @return the total number of changes
         */
        int getTotalNbOfChanges();

        /**
         * @return the number of modifications
         */
        int getnbOfModifications();

        /**
         * Increase the number of modifications
         * @param n number to add
         */
        void changeNumberOfModifications(int n);
    }
}