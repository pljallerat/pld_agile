package ihm;

import model.Intersection;
import model.Request;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.util.Set;

/**
 * Panel containing secondary buttons (add/remove request)
 */
public class BottomRightPanel extends JPanel {
    private Intersection selected;

    private JLabel jl1, jl2, jl3;
    private JButton removePoint;
    private JButton addPickup;
    private JButton addDelivery;
    private JButton addRequest;
    private JTextField pickupDurationTF, deliveryDurationTF;

    private Intersection pickup,delivery;
    private int pickupDuration = 0, deliveryDuration = 0;
    private boolean addRequestAvailable = false;

    /**
     * Constructor of BottomRightPanel
     * @param observer BottomRightPanelObserver allow communication between the BottomRightPanel and the Frame
     */
    public BottomRightPanel(BottomRightPanelObserver observer){
        super();
        BottomRightPanelObserver observer1 = observer;

        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.black));

        GridBagConstraints gcon = new GridBagConstraints();
        gcon.weightx = 1;
        gcon.weighty = 1;

        gcon.gridx = 0;
        gcon.gridy = 0;
        gcon.gridwidth = 4;
        gcon.gridheight = 1;
        gcon.anchor = GridBagConstraints.CENTER;

        jl1 = new JLabel();
        gbl.setConstraints(jl1,gcon);
        add(jl1);

        gcon.gridx = 4;
        jl2 = new JLabel();
        gbl.setConstraints(jl2,gcon);
        add(jl2);

        gcon.gridx = 0;
        gcon.gridy = 1;
        gcon.gridwidth = 8;
        jl3 = new JLabel();
        gbl.setConstraints(jl3,gcon);
        add(jl3);

        gcon.gridx = 0;
        gcon.gridy = 2;
        JLabel jl4 = new JLabel("================= Ajouter une nouvelle requête =====================");
        gbl.setConstraints(jl4,gcon);
        add(jl4);

        gcon.gridx = 2;
        gcon.gridy = 3;
        gcon.gridwidth = 2;
        JLabel jl5 = new JLabel("Pickup Time");
        gbl.setConstraints(jl5,gcon);
        add(jl5);

        gcon.gridx = 4;
        JLabel jl6 = new JLabel("Delivery Time");
        gbl.setConstraints(jl6,gcon);
        add(jl6);

        gcon.insets = new Insets(5,5,5,5);
        gcon.fill = GridBagConstraints.BOTH;

        Icon icon = new ImageIcon("images/pick.PNG");
        addPickup = new JButton(icon);
        addPickup.setText("Pickup");
        addPickup.setMargin(new Insets(5,5,5,5));
        addPickup.setEnabled(false);
        addPickup.addActionListener(e -> {
            if(selected != null) {
                pickup = selected;
                observer.addPickup(pickup);
                if(delivery != null && pickupDuration > 0 && deliveryDuration > 0 && addRequestAvailable)
                    addRequest.setEnabled(true);
            }
        });
        gcon.gridx = 0;
        gcon.gridy = 3;
        gcon.gridwidth = 1;
        gcon.gridheight = 2;
        gbl.setConstraints(addPickup,gcon);
        add(addPickup);

        Icon icon2 = new ImageIcon("images/dlv.PNG");
        addDelivery = new JButton(icon2);
        addDelivery.setText("Delivery");
        addDelivery.setMargin(new Insets(5,5,5,-1));
        addDelivery.setEnabled(false);
        addDelivery.addActionListener(e -> {
            if(selected != null) {
                delivery = selected;
                observer.addDelivery(delivery);
                if(pickup != null && pickupDuration >= 0 && deliveryDuration >= 0 && addRequestAvailable)
                    addRequest.setEnabled(true);
            }
        });
        gcon.gridx = 1;
        gbl.setConstraints(addDelivery,gcon);
        add(addDelivery);

        gcon.fill = GridBagConstraints.HORIZONTAL;
        gcon.insets = new Insets(0,0,0,0);

        pickupDurationTF = new JTextField();
        pickupDurationTF.setEnabled(false);
        pickupDurationTF.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                warn();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                warn();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                warn();
            }

            public void warn(){
                try{
                    if(pickupDurationTF.getText().length() > 0 && Integer.parseInt(pickupDurationTF.getText()) > 0) {
                        pickupDuration = Integer.parseInt(pickupDurationTF.getText());
                        if (deliveryDuration >= 0 && pickup != null && delivery != null && addRequestAvailable)
                            addRequest.setEnabled(true);
                    }else{
                        pickupDuration = 0;
                        addRequest.setEnabled(false);
                    }
                }catch (Exception e){}
            }
        });
        gcon.gridx = 2;
        gcon.gridy = 4;
        gcon.gridheight = 1;
        gbl.setConstraints(pickupDurationTF,gcon);
        add(pickupDurationTF);

        gcon.fill = GridBagConstraints.NONE;
        gcon.gridx = 3;
        JLabel jl7 = new JLabel("s");
        gbl.setConstraints(jl7,gcon);
        add(jl7);

        gcon.fill = GridBagConstraints.HORIZONTAL;

        deliveryDurationTF = new JTextField();
        deliveryDurationTF.setEnabled(false);
        deliveryDurationTF.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                warn();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                warn();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                warn();
            }

            public void warn(){
                try {
                    if (deliveryDurationTF.getText().length() > 0 && Integer.parseInt(deliveryDurationTF.getText()) > 0){
                        deliveryDuration = Integer.parseInt(deliveryDurationTF.getText());
                    if (pickupDuration >= 0 && pickup != null && delivery != null && addRequestAvailable)
                        addRequest.setEnabled(true);
                }else{
                        deliveryDuration = 0;
                        addRequest.setEnabled(false);
                    }
                }catch (Exception e){}
            }
        });
        gcon.gridx = 4;
        gbl.setConstraints(deliveryDurationTF,gcon);
        add(deliveryDurationTF);

        gcon.fill = GridBagConstraints.NONE;
        gcon.gridx = 5;
        JLabel jl8 = new JLabel("s");
        gbl.setConstraints(jl8,gcon);
        add(jl8);

        gcon.fill = GridBagConstraints.BOTH;
        gcon.insets = new Insets(5,5,5,5);

        Icon icon3 = new ImageIcon("images/addP.PNG");
        addRequest = new JButton(icon3);
        addRequest.setText("Ajouter la requête");
        addRequest.setEnabled(false);
        addRequest.addActionListener(e -> {
            observer.changeNumberOfModifications(1);  //+1 for the number of added request
            observer.incrNbOfTotalChanges();    //+1 for the total number of request
            observer.addRequest(new Request(pickupDuration,deliveryDuration,pickup,delivery));
        });
        gcon.gridx = 6;
        gcon.gridy = 3;
        gcon.gridheight = 2;
        gbl.setConstraints(addRequest,gcon);
        add(addRequest);

        gcon.gridx = 0;
        gcon.gridy = 5;
        gcon.gridwidth = 8;
        gcon.gridheight = 1;
        gcon.fill = GridBagConstraints.NONE;
        gcon.insets = new Insets(0,0,0,0);
        JLabel jl9 = new JLabel("=========================================================");
        gbl.setConstraints(jl9,gcon);
        add(jl9);

        Icon icon4 = new ImageIcon("images/remove.PNG");
        removePoint = new JButton(icon4);
        removePoint.setText("Retirer ce point et son point associé");
        removePoint.setEnabled(false);
        removePoint.addActionListener(e -> {
            observer.changeNumberOfModifications(1); //+1 for the number of added request
            observer.incrNbOfTotalChanges();    //+1 for the total number of request$
            observer.deleteRequest(selected);
        });
        gcon.gridx = 0;
        gcon.gridy = 6;
        gcon.fill = GridBagConstraints.BOTH;
        gcon.insets = new Insets(5,5,10,5);
        gbl.setConstraints(removePoint,gcon);
        add(removePoint);
    }

    /**
     * Manage the display of informations about an intersection
     * @param i Intersection to display
     * @param linkedSegments Set<String> Contains the names of the roads concern by the intersection
     */
    public void displayIntersection(Intersection i, Set<String> linkedSegments){
        if(i != null) {
            selected = i;
            String str = "Rue adjacentes : ";
            for (String s : linkedSegments)
                str += s + " - ";
            jl1.setText("Latitude : " + i.getLatitude());
            jl2.setText("Longitude : " + i.getLongitude());
            jl3.setText(str);
        }
    }

    /**
     * Set that the user now can add a request or not
     * @param available if adding is available
     */
    public void setAddRequestAvailable(boolean available) {
        this.addRequestAvailable = available;
        if(available){
            addPickup.setEnabled(true);
            addDelivery.setEnabled(true);
            pickupDurationTF.setEnabled(true);
            deliveryDurationTF.setEnabled(true);
        }else{
            addPickup.setEnabled(false);
            addDelivery.setEnabled(false);
            pickupDurationTF.setEnabled(false);
            deliveryDurationTF.setEnabled(false);
            addRequest.setEnabled(false);
        }
    }

    /**
     * Set that the user now can remove a request or not
     * @param available if removing is available
     */
    public void setRemovePointAvailable(boolean available){
        removePoint.setEnabled(available);
    }

    /**
     * Reset the new request parameters
     */
    public void resetNewRequest(){
        pickup = null;
        delivery = null;
        pickupDuration = 0;
        deliveryDuration = 0;
        pickupDurationTF.setText("");
        deliveryDurationTF.setText("");
    }

    /**
     * Interface which manage interaction between the BottomRightPanel and the Frame
     */
    public interface BottomRightPanelObserver {
        /**
         * Specifying the pickup of the request to add
         * @param i intersection of the pickup
         */
        void addPickup(Intersection i);

        /**
         * Specifying the delivery of the request to add
         * @param i intersection of the delivery
         */
        void addDelivery(Intersection i);

        /**
         * Request to add a request
         * @param r request to add
         */
        void addRequest(Request r);

        /**
         * Increase the number of modifications
         * @param n new number of modifications
         */
        void changeNumberOfModifications(int n);

        /**
         * Increment the number of total changes
         */
        void incrNbOfTotalChanges();

        /**
         * Ask to remove a request, for a point
         * @param i intersection of the pickup or delivery of the request to remove
         */
        void deleteRequest(Intersection i);
    }
}
