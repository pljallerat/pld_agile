package ihm;

import controller.ViewObserver;
import javafx.util.Pair;
import model.Intersection;
import model.PlanningRequest;
import model.Request;
import model.Segment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Set;

/**
 * The Frame class represents the application frame
 */
public class Frame extends JFrame implements RightPanel.RightPanelObserver,
        BottomLeftPanel.BottomLeftPanelObserver, BottomRightPanel.BottomRightPanelObserver,
        Map.MapObserver {
    private ViewObserver observer;

    private Map map;
    private RightPanel rightPanel;
    private BottomLeftPanel bottomLeftPanel;
    private BottomRightPanel bottomRightPanel;
    private double mapWidth;
    private double mapHeigth;
    private int nbOfModifications;
    private int totalNbOfChanges;

    /**
     * Constructor of the Frame class
     * Set general parameter of the Frame
     *
     * @param observer ViewObserver allow interaction between IHM and controler
     */
    public Frame(ViewObserver observer) {
        this.observer = observer;

        map = new Map(this);
        rightPanel = new RightPanel(this);
        bottomRightPanel = new BottomRightPanel(this);
        bottomLeftPanel = new BottomLeftPanel(this);

        setTitle("Pickup Deliveries");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        setLayout(null);
        setVisible(true);

        mapHeigth = getHeight() * 0.8;
        mapWidth = getWidth() * 0.63;
        setMapWidthAndHeight();
        map.setBounds(0, 0, (int) (getWidth() * 0.63), (int) (getHeight() * 0.8));
        rightPanel.setBounds((int) (getWidth() * 0.63), 0, (int) (getWidth() * 0.37), (int) (getHeight() * 0.55));
        bottomLeftPanel.setBounds(0, (int) (getHeight() * 0.8), (int) (getWidth() * 0.63), (int) (getHeight() * 0.2));
        bottomRightPanel.setBounds((int) (getWidth() * 0.63), (int) (getHeight() * 0.55), (int) (getWidth() * 0.36), (int) (getHeight() * 0.41));
        add(map);
        add(rightPanel);
        add(bottomLeftPanel);
        add(bottomRightPanel);
    }

    public void setMapWidthAndHeight() {
        map.setMapWidthAndHeigth(mapWidth, mapHeigth);
    }

    /**
     * Display the map in the application
     *
     * @param segments Set of Segments which corresponds to all the roads to put in the map
     * @param minMax   Float tab which shows the extremun (max & min) of the latitude and the longitude.
     */
    public void drawMap(Set<Segment> segments, float[] minMax) {
        bottomLeftPanel.setLoadRequestButtonEnable(true);
        bottomLeftPanel.setExecuteRequestButtonEnable(false);
        bottomLeftPanel.setJL1("Map chargée !");
        bottomRightPanel.resetNewRequest();
        map.setSegmentList(segments, minMax);
        map.repaint();
    }

    /**
     * Display pickup and delivery points
     *
     * @param planning PlanningRequest ==> Request to display
     */
    public void drawPoints(PlanningRequest planning) {
        if (planning != null) {
            bottomLeftPanel.setExecuteRequestButtonEnable(true);
            bottomLeftPanel.setJL1("Requête chargée !");
            bottomRightPanel.resetNewRequest();
            map.setPoints(planning);
            map.repaint();
        }
        rightPanel.setPlanningRequest(planning);
        rightPanel.repaint();
    }

    /**
     * Draw the found path
     *
     * @param path ordered list of intersections
     */
    public void drawPath(List<Intersection> path) {
        if (path != null) {
            map.setPath(path);
            bottomLeftPanel.setJL1("Calcul terminé !");
            bottomLeftPanel.setExecuteRequestButton("Calculer le trajet");
            bottomLeftPanel.setThreadState(false);
            bottomRightPanel.setAddRequestAvailable(true);
            repaint();
        }
    }

    /**
     * Show the found arrival times
     *
     * @param arrivalTimes list of intersection and their arrival times
     */
    public void setArrivalTimes(List<Pair<Long, Intersection>> arrivalTimes) {
        map.setNumber(arrivalTimes);
        rightPanel.setArrivalTimes(arrivalTimes);
        repaint();
    }

    /**
     * Show an error message
     *
     * @param message message of the error
     */
    public void showError(String message) {
        JOptionPane.showMessageDialog(this,
                message,
                "Warning",
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Undo last change
     */
    @Override
    public void undo() {
        observer.undo();
    }

    /**
     * Redo last undone change
     */
    @Override
    public void redo() {
        observer.redo();
    }

    @Override
    public int getnbOfModifications() {
        return nbOfModifications;
    }

    /**
     * Increase the number of modifications of <code>n</code>
     *
     * @param n new number of modifications
     */
    @Override
    public void changeNumberOfModifications(int n) {
        nbOfModifications += n;
    }

    /**
     * Increment the number of total changes
     */
    @Override
    public void incrNbOfTotalChanges() {
        totalNbOfChanges++;
    }

    public int getTotalNbOfChanges() {
        return totalNbOfChanges;
    }

    /**
     * Reset to 0 the number of total changes
     */
    public void resetTotalNbOfChanges() {
        totalNbOfChanges = 0;
    }

    /**
     * Reset to 0 the number of modifications
     */
    public void resetNbOfModification() {
        nbOfModifications = 0;
    }

    /**
     * Ask controller to load a map
     *
     * @param filePath Access path to the map
     */
    @Override
    public void loadMap(String filePath) {
        observer.loadMap(filePath);
        resetNbOfModification();
        resetTotalNbOfChanges();
        rightPanel.warn(this); //met à jour l'affichage des boutons undo-redo
        bottomRightPanel.setAddRequestAvailable(false);
    }

    /**
     * Ask controller to load a request
     *
     * @param filePath Access path to the request
     */
    @Override
    public void loadRequest(String filePath) {
        observer.loadRequest(filePath);
        bottomRightPanel.setAddRequestAvailable(false);
        bottomRightPanel.setRemovePointAvailable(false);
    }

    /**
     * request to calculate the best path
     */
    @Override
    public void executeRequest() {
        map.removeNewRequest();
        bottomRightPanel.resetNewRequest();
        observer.executeRequest();
    }

    /**
     * Stop the calculation
     */
    @Override
    public void stopThread() {
        observer.stopThread();
    }

    /**
     * Ask Panel to display information about intersections
     *
     * @param i Intersection to display
     */
    @Override
    public void displayIntersectionInTab(Intersection i) {
        rightPanel.selectIntersection(i);
    }

    /**
     * Show the information of an intersection
     * @param i intersection
     * @param s segments linked to this intersection
     */
    @Override
    public void displayInfoIntersection(Intersection i, Set<String> s) {
        bottomRightPanel.displayIntersection(i, s);
        if (map.isDeleteAvailable())
            bottomRightPanel.setRemovePointAvailable(true);
        else
            bottomRightPanel.setRemovePointAvailable(false);
    }

    /**
     * Highlight an intersection on the map view
     * @param i intersection to highlight
     */
    @Override
    public void highlightIntersectionInMap(Intersection i) {
        map.setSelectedIntersection(i);
    }

    /**
     * Add (in visualization) a pickup
     * @param i intersection of the pickup
     */
    @Override
    public void addPickup(Intersection i) {
        map.addPickup(i);
        map.repaint();
    }

    /**
     * Add (in visualization) a delivery
     * @param i intersection of the delivery
     */
    @Override
    public void addDelivery(Intersection i) {
        map.addDelivery(i);
        map.repaint();
    }

    /**
     * Add a request
     * @param r request to add
     */
    @Override
    public void addRequest(Request r) {
        rightPanel.warn(this); //met à jour l'affichage des boutons undo-redo
        observer.addRequest(r);
    }

    /**
     * Delete a request
     * @param i intersection of the pickup or delivery of the request to remove
     */
    @Override
    public void deleteRequest(Intersection i) {
        rightPanel.warn(this); //met à jour l'affichage des boutons undo-redo
        Request r = map.findRequest(i);
        observer.removeRequest(r);
    }
}
