package ihm;

import controller.Debug;
import javafx.util.Pair;
import model.Intersection;
import model.PlanningRequest;
import model.Request;
import model.Segment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

/**
 * The Map class manages the map display and the interaction with the map
 *
 * @implements MouseMotionListener
 * @implements MouseWheelListener
 * @implements MouseListener
 */
public class Map extends JPanel implements MouseMotionListener, MouseWheelListener, MouseListener {

    private static final String TAG = "Map";
    private static final int POINT_SIZE = 16;

    //Colors to use
    private static final Color[] COLORS = new Color[]{
            new Color(0xf44336),
            new Color(0xe81e62),
            new Color(0xab47bc),
            new Color(0x7e57c2),
            new Color(0x5c6bc0),
            new Color(0x42a5f5),
            new Color(0x29b6f6),
            new Color(0x26c6da),
            new Color(0x26a69a),
            new Color(0x66bb6a),
            new Color(0x9ccc65),
            new Color(0xd4e157),
            new Color(0xffee58),
            new Color(0xffca28),
            new Color(0xffa726),
            new Color(0xff7043),
            new Color(0x8d6e63)
    };

    //Managing map colors
    private static final Color BACKGROUND_COLOR = new Color(0xB0B0B0);
    private static final Color SEGMENT_COLOR = new Color(0xFFFFFF);
    private static final Color PATH_COLOR = new Color(0x0288d1);
    private static final Color CURSOR_COLOR = new Color(0xFF0000);
    private static final Color LEGEND_BACKGROUND_COLOR = new Color(0x70ffffff, true);


    private static final int SEGMENT_WITH = 3;
    private static final int PATH_WITH = 5;

    //Interface which manages interaction between the Map and the Frame
    private Map.MapObserver observer;

    private int zoom;
    private float lonOffset, latOffset;
    private float minLon, maxLon, minLat, maxLat;
    private int posX = 0, posY = 0;
    private Intersection overIntersection = null;
    private Intersection selectedIntersection = null;
    private boolean selectedIntersectionIsOnPath = false;

    private Set<Segment> segmentList = null;
    private PlanningRequest request = null;
    private Request newRequest = null;
    private List<Color> colors = new ArrayList<>();
    private List<Intersection> path = null;
    private List<Pair<Long, Intersection>> orderAddress = null;

    private double mapWidth;
    private double mapHeigth;

    private Rectangle legendBounds;

    /**
     * Constructor of the Map class
     * Initialise les ActionListener
     *
     * @param observer MapObserver allow communictaion between the Map and the Frame
     */
    public Map(MapObserver observer) {
        this.observer = observer;
        addMouseMotionListener(this);
        addMouseWheelListener(this);
        addMouseListener(this);
    }

    /**
     * Manage the display in the panel with Graphics object
     *
     * @param g Graphics in the panel
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // We transform the Graphics into a Graphics2D
        Graphics2D g2 = (Graphics2D) g;
        // We apply a method to avoid the staircase effect on the graph
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // We manage the offsets and the zoom to prevent the map from leaving the window
        manageValues();

        // Draw the background
        g2.setColor(BACKGROUND_COLOR);
        g2.fillRect(0, 0, g2.getClipBounds().width, g2.getClipBounds().height);

        // If a card is saved, we display it
        g2.setStroke(new BasicStroke(SEGMENT_WITH));
        g2.setColor(SEGMENT_COLOR);
        if (segmentList != null && segmentList.size() != 0) {
            for (Segment s : segmentList) {
                drawRoad(g2, s.getOrigin(), s.getDestination());
            }
        }

        g2.setColor(PATH_COLOR);
        g2.setStroke(new BasicStroke(PATH_WITH));
        if (path != null && path.size() != 0) {
            //g2.setStroke(new BasicStroke(3f));
            for (int i = 0; i < path.size() - 1; i++) {

                drawRoad(g2, path.get(i), path.get(i + 1));
            }
            drawRoad(g2, path.get(0), path.get(path.size() - 1));
        }

        // If a request is recorded, we display it
        if (request != null && request.getRequests().size() != 0) {
            drawDepot(g2, request.getDepot().getAddress());
            for (int i = 0; i < request.getRequests().size(); i++) {
                g.setColor(colors.get(i));
                drawPickup(g2, request.getRequests().get(i).getPickupAddress());
                drawDelivery(g2, request.getRequests().get(i).getDeliveryAddress());
            }

            if (orderAddress != null) {
                g2.setColor(Color.black);
                drawNumber(g2);
            }
        }

        if(newRequest != null){
            if(request != null)
                g.setColor(colors.get(request.getRequests().size()));
            else
                g.setColor(COLORS[0]);
            if(newRequest.getPickupAddress() != null)
                drawPickup(g2, newRequest.getPickupAddress());
            if(newRequest.getDeliveryAddress() != null)
                drawDelivery(g2, newRequest.getDeliveryAddress());
        }

        // If the mouse is on an intersection, we put it forward
        if (overIntersection != null) {
            g2.setColor(Color.yellow);
            drawCursor(g2, overIntersection);
        }

        // If an intersection of the request is selected, we put it forward
        if(selectedIntersection != null && selectedIntersectionIsOnPath){
            drawArrowToSelect(
                    g2,
                    selectedIntersection.getLongitude(),
                    selectedIntersection.getLatitude(),
                    20,
                    20);
            drawSelection(g2, overIntersection);
        }else if (selectedIntersection != null) {
            drawSelection(g2, overIntersection);
        }

        /*// Si une intersection de la requete est selectionnee, on la met encore plus en avant
        if(selectedIntersection != null && request != null && request.getRequests().size() != 0) {
            // select depot
            for(int i=0;i<request.getRequests().size();i++) {
                if(request.getRequests().get(i).getPickupAddress().equals(selectedIntersection)
                        || request.getRequests().get(i).getDeliveryAddress().equals(selectedIntersection)) {
                    drawArrowToSelect(
                            g2,
                            selectedIntersection.getLongitude(),
                            selectedIntersection.getLatitude(),
                            20,
                            20);
                }
            }
            if(selectedIntersection.equals(request.getDepot().getAddress())) {
                    drawArrowToSelect(
                            g2,
                            selectedIntersection.getLongitude(),
                            selectedIntersection.getLatitude(),
                            20,
                            20);
            }
        }*/

        drawLegend(g2);
    }


    /**
     * Display a road between 2 intersections
     *
     * @param g  Graphics2D to use for display
     * @param i1 Intersection 1
     * @param i2 Intersection 2
     */
    void drawRoad(Graphics2D g, Intersection i1, Intersection i2) {
        //Draws a line, using the current color
        g.drawLine(
                normalizeLongitude(i1.getLongitude()),
                normalizeLatitude(i1.getLatitude()),
                normalizeLongitude(i2.getLongitude()),
                normalizeLatitude(i2.getLatitude()));
    }

    /**
     *  Display the number which allow the identify the running order of the delivery and pickup point
     *
     * @param g Graphics2D use for display
     */
    void drawNumber(Graphics2D g) {
        Font font = new Font("Serif", Font.PLAIN, 25);
        g.setFont(font);
        Intersection intersection;
        double latitude;
        double longitude;
        for (int i = 0; i < orderAddress.size(); i++) {
            intersection = orderAddress.get(i).getValue();
            // Draw a string such that its base line is at x, y
            if (i != orderAddress.size() - 1) {
                g.drawString(Integer.toString(i + 1),
                        (int)checkNumberLongitude(normalizeLongitude(intersection.getLongitude())),
                        (int)checkNumberLatitude(normalizeLatitude(intersection.getLatitude())));
            } else {
                g.drawString("Dépot",
                        (int)checkStringLongitude(normalizeLongitude(intersection.getLongitude())),
                        (int)checkStringLatitude(normalizeLatitude(intersection.getLatitude())));
            }
        }
    }

    /**
     * Manage Map Width And Heigth
     * @param width
     * @param height
     */
    public void setMapWidthAndHeigth(double width, double height) {
        mapWidth = width;
        mapHeigth = height;
    }

    /**
     * Manage the display of the number (for the running order) in order they stay in the map
     * ONLY FOR HEIGHT
     *
     * @param position
     * @return new position
     */
    public double checkNumberLatitude(double position){
        if(position > mapHeigth - 32){
            position = mapHeigth - 20;
        } else {
            position += 32;
        }
        return position;
    }

    /**
     * Manage the display of the number (for the running order) in order they stay in the map
     * ONLY OFR WIDTH
     *
     * @param position
     * @return new position
     */
    public double checkNumberLongitude(double position){
        if (position > 10) {
            position -= 25;
        } else {
            position += 5;
        }
        return position;
    }

    /**
     * Manage the display the "Dépot" word in order it stays in the map
     * ONLY FOR HEIGHT
     *
     * @param position
     * @return new position
     */
    public double checkStringLatitude(double position){
        if(position > mapHeigth - 32){
            position = mapHeigth - 25;
        } else {
            position += 32;
        }
        return position;
    }

    /**
     * Manage the display the "Dépot" word in order it stays in the map
     * ONLY FOR WIDTH
     *
     * @param position
     * @return new position
     */
    public double checkStringLongitude(double position){
        if (position > 10) {
            position -= 65;
        } else {
            position += 10;
        }
        return position;
    }

    private void drawPickup(Graphics2D g, int x, int y) {
        g.fillRect(
                x - (POINT_SIZE / 2),
                y - (POINT_SIZE / 2),
                POINT_SIZE,
                POINT_SIZE);
    }

    /**
     * Display a pickup point
     *
     * @param g Graphics2D to use
     * @param i Intersection to display
     */
    void drawPickup(Graphics2D g, Intersection i) {
        drawPickup(g, normalizeLongitude(i.getLongitude()), normalizeLatitude(i.getLatitude()));
        /*Font font = new Font("Serif", Font.PLAIN, 25);
        g.setFont(font);
        g.drawString("Pickup",
                normalizeLongitude(i.getLongitude()) + 15,
                normalizeLatitude(i.getLatitude()) + 8);*/
    }

    /**
     * Affiche un point de livraison
     *
     * @param g Graphics2D à utiliser
     * @param i Intersection à afficher
     */
    void drawDelivery(Graphics2D g, Intersection i) {
        drawDelivery(g,
                normalizeLongitude(i.getLongitude()),
                normalizeLatitude(i.getLatitude()));
        /*Font font = new Font("Serif", Font.PLAIN, 25);
        g.setFont(font);
        g.drawString("Delivery",
                normalizeLongitude(i.getLongitude()) + 15,
                normalizeLatitude(i.getLatitude()) + 8);*/
    }

    private void drawDelivery(Graphics2D g, int x, int y) {
        g.fillOval(
                x - (POINT_SIZE / 2),
                y - (POINT_SIZE / 2),
                POINT_SIZE,
                POINT_SIZE);
    }

    /**
     * Display depot
     *
     * @param g Graphics 2D to use
     * @param i Intersection to display
     */
    void drawDepot(Graphics2D g, Intersection i) {
        drawDepot(g, normalizeLongitude(i.getLongitude()), normalizeLatitude(i.getLatitude()));
    }

    private void drawDepot(Graphics2D g, int x, int y) {
        g.setColor(Color.red);
        g.fillOval(x - 10, y - 10,
                20, 20);
        g.setColor(Color.LIGHT_GRAY);
        g.fillOval(x - 8, y - 8,
                16, 16);
        g.setColor(Color.red);
        g.fillOval(x - 5, y - 5,
                10, 10);
    }

    void drawCursor(Graphics2D g, Intersection i) {
        g.setColor(CURSOR_COLOR);
        g.drawOval(
                normalizeLongitude(i.getLongitude()) - (POINT_SIZE / 2),
                normalizeLatitude(i.getLatitude()) - (POINT_SIZE / 2),
                POINT_SIZE,
                POINT_SIZE);
    }

    void drawSelection(Graphics2D g, Intersection i) {
        g.setColor(CURSOR_COLOR);
        g.drawOval(
                normalizeLongitude(selectedIntersection.getLongitude()) - (POINT_SIZE / 2),
                normalizeLatitude(selectedIntersection.getLatitude()) - (POINT_SIZE / 2),
                POINT_SIZE,
                POINT_SIZE);
    }

    /**
     * Draw an arrow over the selected point
     *
     * @param g Graphics to use
     * @param xi Coordonnee X no standardized
     * @param yi Coordonnee Y no standardized
     * @param largeur arrow width
     * @param hauteur arrow height
     */
    void drawArrowToSelect (Graphics2D g, float xi, float yi, int largeur, int hauteur)
    {
        g.setColor(Color.red);
        int x = normalizeLongitude(xi)-9;
        int y = normalizeLatitude(yi)-30;
        largeur = largeur / 3;
        hauteur = hauteur / 3;

        g.fillRect(x + largeur, y,
                largeur, 2 * hauteur);

        int abs[] = new int[]{x,
                x + (3 * largeur),
                x + (largeur * 3 / 2)};
        int ord[] = new int[]{y + (2 * hauteur),
                y + (2 * hauteur),
                y + (3 * hauteur)};

        g.fillPolygon(abs, ord, 3);
}

    /**
     * Convert a latitude into a coordinate in the graph
     *
     * @param f Float - Latitude to convert
     * @return int - Latitude converted
     */
    int normalizeLatitude(float f) {
        return (int) ((latOffset - f) * zoom);
    }

    /**
     * Convert a pixel coordinate into a latitude
     *
     * @param p int - pixel represent by the latitude to convert
     * @return float - pixel latitude
     */
    float pixelToLatitude(int p) { return latOffset-p/(float)zoom; }

    /**
     * Convert a latitude into a coordinate in the graph
     *
     * @param f Float - Longitude to convert
     * @return int - Longitude converted
     */
    int normalizeLongitude(float f) {
        return (int) ((f - lonOffset) * zoom);
    }

    /**
     * Convert a pixel coordinate into a longitude
     *
     * @param p int - pixel represent by the longitude to convert
     * @return float - pixel longitude
     */
    float pixelToLongitude(int p) { return p/(float)zoom + lonOffset; }

    void setNumber(List<Pair<Long, Intersection>> intersectionsList) {
        orderAddress = intersectionsList;
    }

    /**
     * Save the map to display with his values
     *
     * @param s      Set<Segment> - Map to save
     * @param minMax float[] - Tab which stocks the minimals and maximals values of the longitudes and latitudes intersections
     *               offset[0] = maxLat;
     *               offset[1] = minLat;
     *               offset [2] = maxLong;
     *               offset[3] = minLong;
     */
    void setSegmentList(Set<Segment> s, float[] minMax) {
        segmentList = s;
        request = null;
        path = null;
        orderAddress = null;
        newRequest = null;
        this.minLon = minMax[3];
        this.maxLon = minMax[2];
        this.minLat = minMax[1];
        this.maxLat = minMax[0];
        lonOffset = minLon;
        latOffset = maxLat;
        // On adapte le zoom pour que la hauteur de la carte corresponde à la hauteur de la boite carte
        zoom = (int) (this.getHeight() / (maxLat - minLat));

    }

    /**
     * Enregistre une requête à afficher
     *
     * @param planning PlanningRequest - Requête à enregistrer
     */
    void setPoints(PlanningRequest planning) {
        path = null;
        orderAddress = null;
        newRequest = null;
        this.request = planning;

        // Pour chaque paire de chargement/livraison on lui attribue une couleur
        colors.addAll(Arrays.asList(COLORS));
        for (int i = colors.size(); i < planning.getRequests().size(); i++)
            colors.add(new Color(
                    (int) (Math.random() * 255),
                    (int) (Math.random() * 255),
                    (int) (Math.random() * 255)));
    }

    void setPath(List<Intersection> path) {
        this.path = path;
    }

    void addPickup(Intersection pickup){
        if(newRequest == null)
            newRequest = new Request(0,0,pickup,null);
        else
            newRequest.setPickupAddress(pickup);
    }

    void addDelivery(Intersection delivery){
        if(newRequest == null)
            newRequest = new Request(0,0,null,delivery);
        else
            newRequest.setDeliveryAddress(delivery);
    }

    void removeNewRequest(){
        newRequest = null;
    }

    /**
     * Manage the offset to avoid to display the map out of the frame
     * Manage the zoom to block the zoom out when there is the no more part of the map ot display
     */
    void manageValues() {
        if (lonOffset < minLon) {
            lonOffset = minLon;
        } else if ((maxLon - minLon) * zoom < this.getWidth()) {
            lonOffset = minLon;
        } else if (lonOffset > maxLon - (this.getWidth() / (float) zoom)) {
            lonOffset = maxLon - (this.getWidth() / (float) zoom);
        }
        if (latOffset > maxLat) {
            latOffset = maxLat;
        } else if ((maxLat - minLat) * zoom < this.getHeight()) {
            latOffset = maxLat;
        } else if (latOffset < minLat + (this.getHeight() / (float) zoom)) {
            latOffset = minLat + (this.getHeight() / (float) zoom);
        }
        if ((maxLat - minLat) * zoom < this.getHeight()) {
            zoom = (int) (this.getHeight() / (maxLat - minLat));
        }
    }

    /**
     * Return true if the selected intersection can be deleted
     * (only on the route, no the depot, the rout is already calculated)
     * @return
     */
    boolean isDeleteAvailable(){
        if(request == null || selectedIntersection.equals(request.getDepot().getAddress()) || path == null)
            return false;
        return selectedIntersectionIsOnPath;
    }

    /**
     * Return the Request which contains the Intersection in parameter
     *
     * @param i Intersection
     * @return Request
     */
    Request findRequest(Intersection i){
        for(Request r : request.getRequests()){
            if(r.getPickupAddress().equals(i)
                    || r.getDeliveryAddress().equals(i)) {
                return r;
            }
        }
        return  null;
    }

    void drawLegend(Graphics2D g) {
        // 1st draw the background
        if (legendBounds == null) {
            final int width = getWidth() / 5;
            final int height = getHeight() / 7;
            final int margin = width / 20;
            legendBounds = new Rectangle(getWidth() - width - margin,
                    getHeight() - height - margin,
                    width,
                    height);
        }


        final int padding = legendBounds.width / 20;
        final int textCount = 3;
        final int textHeight = (legendBounds.height - 2 * padding) / textCount;
        final int textSize = 24;
        final Font font = new Font("Sans-serif", Font.PLAIN, textSize);

        g.setColor(LEGEND_BACKGROUND_COLOR);
        g.fillRect(legendBounds.x, legendBounds.y, legendBounds.width, legendBounds.height);

        final int iconX = legendBounds.x + padding + 10;
        final int textX = iconX + 25;

        // Draw depot legend
        int currY = legendBounds.y + padding + textHeight / 2;
        drawDepot(g, iconX, currY);
        g.setColor(Color.black);
        g.setFont(font);
        g.drawString("Depot", textX, currY + textSize / 2);

        // Draw delivery legend
        currY += textHeight;
        g.setColor(Color.DARK_GRAY);
        drawDelivery(g, iconX, currY);
        g.setColor(Color.black);
        g.setFont(font);
        g.drawString("Delivery", textX, currY + textSize / 2);

        // Draw pickup legend
        currY += textHeight;
        g.setColor(Color.DARK_GRAY);
        drawPickup(g, iconX, currY);
        g.setColor(Color.black);
        g.setFont(font);
        g.drawString("Pickup", textX, currY + textSize / 2);
    }

    /**
     * Manage the mpa movement when the user is doing a drga and drop
     *
     * @param e MouseEvent
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        // We get the coordinates of the mouse
        int mX = MouseInfo.getPointerInfo().getLocation().x - getLocationOnScreen().x;
        int mY = MouseInfo.getPointerInfo().getLocation().y - getLocationOnScreen().y;

        // We look at the displacement made since the last entry in the method and we update the offsets accordingly
        if (posX != 0 && posY != 0) {
            lonOffset += (posX - mX) / (float) zoom;
            latOffset -= (posY - mY) / (float) zoom;
            posX = mX;
            posY = mY;
        } else {
            posX = mX;
            posY = mY;
        }
        repaint();
    }

    /**
     * Manage map zoom when the user is using the mouse wheel
     *
     * @param e MouseWheelEvent
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        int mX = MouseInfo.getPointerInfo().getLocation().x - getLocationOnScreen().x;
        int mY = MouseInfo.getPointerInfo().getLocation().y + getLocationOnScreen().y;
        float offLon = pixelToLongitude(mX);
        float offLat = pixelToLongitude(mY);

        zoom -= e.getPreciseWheelRotation() * 5000;
        offLat -= pixelToLongitude(mY);
        offLon -= pixelToLongitude(mX);
        lonOffset += offLon;
        latOffset -= offLat;

        repaint();
    }

    public void setSelectedIntersection(Intersection i) {
        selectedIntersection = i;
        selectedIntersectionIsOnPath = true;
        observer.displayInfoIntersection(i,getLinkedSegments());
        repaint();
    }

    /**
     * Pick-up the intersection under the mouse pointer and stock it the Intersectio i in parameter
     *
     *
     * @param select int, Intersection à mettre à jour
     *               0 : overIntersection
     *               1 : selectedIntersection
     */
    public boolean overIntersection(int select) {
        Intersection i;
        if (segmentList == null)
            return false;
        i = null;
        // We get the coordinates of the mouse on the window
        int mX = MouseInfo.getPointerInfo().getLocation().x - getLocationOnScreen().x;
        int mY = MouseInfo.getPointerInfo().getLocation().y - getLocationOnScreen().y;

        // We choose the precision of the intersection around the mouse
        // Square of 2 * precision pixels on the side
        int precision = 10;
        // we recover the first intersection of the segments in the requested precision
        // using a sparse matrix implementation for intersections
        for (Segment s : segmentList) {
            if (normalizeLatitude(s.getOrigin().getLatitude()) - precision < mY
                    && mY < normalizeLatitude(s.getOrigin().getLatitude()) + precision
                    && normalizeLongitude(s.getOrigin().getLongitude()) - precision < mX
                    && mX < normalizeLongitude(s.getOrigin().getLongitude()) + precision
                    ) {
                i = s.getOrigin();
                break;
            } else if (normalizeLatitude(s.getDestination().getLatitude()) - precision < mY
                    && mY < normalizeLatitude(s.getDestination().getLatitude()) + precision
                    && normalizeLongitude(s.getDestination().getLongitude()) - precision < mX
                    && mX < normalizeLongitude(s.getDestination().getLongitude()) + precision
                    ) {
                i = s.getDestination();
                break;
            }
        }
        switch (select) {
            case 0:
                overIntersection = i;
                break;
            case 1:
                selectedIntersection = i;
                break;
        }

        return true;
    }

    /**
     * Pick-up all the roads name concern by the selected intersection
     *
     *
     * @return a String Set which contains all the roads name concern by the selected intersection
     */

    public Set<String> getLinkedSegments() {
        Set<String> linkedSegment = new HashSet<>();
        for (Segment s : segmentList) {
            if (s.getOrigin() == selectedIntersection) {
                linkedSegment.add(s.getName());
            }
            if (s.getDestination() == selectedIntersection) {
                linkedSegment.add(s.getName());
            }
        }
        return linkedSegment;
    }

    /**
     * Ask to highlighted the intersection under the mouse pointer
     *
     * @param e MouseEvent
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        if (overIntersection(0))
            repaint();
        if (legendBounds.contains(e.getX(), e.getY())) {
            Debug.i(TAG, "legend hover !!!");
            legendBounds.x = getWidth() - legendBounds.x - legendBounds.width;
            repaint();
        }
    }

    /**
     * Pick-up the Intersection where the user clicked and ask to display the information of the Intersection in the Frame
     *
     * @param e MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        // Save the selected intersection in selectedIntersection
        if (!overIntersection(1))
            return;

        if(isiIntersectionOnPath(selectedIntersection))
            selectedIntersectionIsOnPath = true;
        else
            selectedIntersectionIsOnPath = false;

        // Ask the Frame to display its information
        observer.displayInfoIntersection(selectedIntersection, getLinkedSegments());
        observer.displayIntersectionInTab(selectedIntersection);
        repaint();
    }

    private boolean isiIntersectionOnPath(Intersection intersection){
        if(request != null){
            if(intersection.equals(request.getDepot().getAddress())) {
                return true;
            }else{
                for(int i=0;i<request.getRequests().size();i++) {
                    if(request.getRequests().get(i).getPickupAddress().equals(intersection)
                            || request.getRequests().get(i).getDeliveryAddress().equals(intersection)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * When a mouse clicked is release, particulary after a drag and drop, it reinitializes the posX and posY values
     *
     * @param e MouseEvent
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        posX = 0;
        posY = 0;
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }


    /**
     * Interface which manages interaction between the Map and the Frame
     */
    public interface MapObserver {
        void displayIntersectionInTab(Intersection i);
        void displayInfoIntersection(Intersection i, Set<String> s);
    }
}