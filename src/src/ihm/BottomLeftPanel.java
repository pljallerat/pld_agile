package ihm;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Panel containing the main buttons (load map, load request, calculate path)
 */
public class BottomLeftPanel extends JPanel {
    private BottomLeftPanelObserver observer;

    private JLabel jl1;
    private JButton loadMap;
    private JButton loadRequest;
    private JButton executeRequest;

    private boolean threadState = false;

    /**
     * Constructor of the BottomLeftPanel
     * @param observer BottomLeftPanelObserver allow communication between the BottomLeftPanel and the Frame
     */
    public BottomLeftPanel(BottomLeftPanelObserver observer){
        super();
        this.observer = observer;

        //Bottom left panel
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        setBackground(Color.GRAY);
        setBorder(BorderFactory.createLineBorder(Color.black));

        //Bottom left panel constraints
        GridBagConstraints gcon = new GridBagConstraints();
        gcon.weightx = 1;
        gcon.weighty = 1;

        gcon.gridx = 1;
        gcon.gridy = 0;
        gcon.gridwidth = 1;
        gcon.gridheight = 1;
        gcon.anchor = GridBagConstraints.CENTER;

        //Left label
        jl1 = new JLabel();
        gbl.setConstraints(jl1,gcon);
        add(jl1);

        gcon.gridx = 0;
        gcon.gridy = 1;
        gcon.insets = new Insets(5,5,50,5);
        gcon.fill = GridBagConstraints.BOTH;

        /**
         * Button loadMap
         *
         * @param Filepath
         */
        //icon button loadMap
        Icon icon = new ImageIcon("images/map2.PNG");
        loadMap = new JButton(icon);
        loadMap.setText("Charger une carte");
        loadMap.setFont(new Font("Arial", Font.BOLD, 18));
        loadMap.setMargin(new Insets(5,5,5,5));
        //Button loadMap action
        loadMap.addActionListener(e -> {
            FileDialog fd = new FileDialog(new JFrame());
            fd.setVisible(true);
            File[] f = fd.getFiles();
            if(f.length > 1){
                jl1.setText("Erreur : Plusieurs fichiers spécifiés !");
            }else if(f.length < 1){
                jl1.setText("Erreur : Aucun fichier spécifié !");
            }else{
                jl1.setText("Loading Map");
                //System.out.println(f[0].getAbsolutePath());
                observer.loadMap(f[0].getAbsolutePath());
            }
        });
        gbl.setConstraints(loadMap,gcon);
        add(loadMap);

        gcon.gridx = 1;

        /**
         * Button loadRequest
         *
         * @param Filepath
         */
        //icon button loadRequest
        Icon icon2 = new ImageIcon("images/rq.PNG");
        loadRequest = new JButton(icon2);
        loadRequest.setText("Charger une requête");
        loadRequest.setFont(new Font("Arial", Font.BOLD, 18));
        loadRequest.setEnabled(false);
        //Button loadRequest action
        loadRequest.addActionListener(e -> {
            FileDialog fd = new FileDialog(new JFrame());
            fd.setVisible(true);
            File[] f = fd.getFiles();
            if(f.length > 1){
                jl1.setText("Erreur : Plusieurs fichiers spécifiés !");
            }else if(f.length < 1){
                jl1.setText("Erreur : Aucun fichier spécifié !");
            }else{
                jl1.setText("Loading Request");
                observer.loadRequest(f[0].getAbsolutePath());
            }
        });
        gbl.setConstraints(loadRequest,gcon);
        add(loadRequest);

        gcon.gridx = 2;

        /**
         * Button executeRequest
         */
        //icon button executeRequest (calculer le trajet)/
        Icon icon3 = new ImageIcon("images/road.PNG");
        executeRequest = new JButton(icon3);
        executeRequest.setEnabled(false);
        executeRequest.setText("Calculer le trajet");
        executeRequest.setFont(new Font("Arial", Font.BOLD, 18));
        executeRequest.setEnabled(false);
        //Button executeRequest action
        executeRequest.addActionListener(e -> {
            if(!threadState){
                setJL1("Calcul en cour !");
                threadState = true;
                executeRequest.setText("Annuler le calcul");
                observer.executeRequest();
            }else{
                executeRequest.setText("Calculer le trajet");
                setJL1("Calcul annulé !");
                threadState = false;
                observer.stopThread();
            }
        });
        gbl.setConstraints(executeRequest,gcon);
        add(executeRequest);

    }

    /**
     * Update the status of the button to load request: enable or disable
     * @param b true or false
     */
    public void setLoadRequestButtonEnable(boolean b){
        loadRequest.setEnabled(b);
    }

    /**
     * Update the status of the button to execute request: enable or disable
     * @param b true or false
     */
    public void setExecuteRequestButtonEnable(boolean b){
        executeRequest.setEnabled(b);
    }

    /**
     * Update JLabel 1 text
     * @param str new text
     */
    public void setJL1(String str){
        jl1.setText(str);
    }

    /**
     * Change name of the executeRequest button
     * @param str new nom
     */
    public void setExecuteRequestButton(String str){
        executeRequest.setText(str);
    }

    /**
     * Update Thread status
     * @param b
     */
    public void setThreadState(boolean b){
        threadState = b;
    }

    /**
     * Interface which manage interaction between the Panel and the Frame
     */
    public interface BottomLeftPanelObserver {

        /**
         * Request to load a map
         * @param filePath path to a xml file describing a map
         */
        void loadMap(String filePath);

        /**
         * Request to load a request file
         * @param filePath path to the file
         */
        void loadRequest(String filePath);

        /**
         * Request to calculate the path matching the loaded planning request
         */
        void executeRequest();

        /**
         * Request to stop the calculation
         */
        void stopThread();
    }
}
