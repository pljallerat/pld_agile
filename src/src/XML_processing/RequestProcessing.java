package XML_processing;

import model.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Class used to read a request file
 */
public class RequestProcessing {

    /**
     * Load the request contains in a xml file and generate a planning request object by using the current map intersections
     *
     * @param path path to the request XML file
     * @return a set of PlanningRequest
     * @throws IOException exception thrown by file reading
     * @throws SAXException thrown by xml reading
     * @throws ParserConfigurationException thrown by wrong configurations
     * @throws RequestToLargeException if the map does not contains an intersection id referenced in the request
     * @throws XML_processing.Utils.WrongDocumentInput if the document doesn't contain what it is suppose to
     */
    public static PlanningRequest generateRequestList(String path)
            throws ParserConfigurationException, IOException, SAXException, RequestToLargeException, Utils.WrongDocumentInput {
        Document doc = Utils.parseXml(path);

        //liste des balises <intersection>
        NodeList nList = doc.getElementsByTagName("request");

        NodeList nodeDepot = doc.getElementsByTagName("depot");

        if(nList == null || nList.getLength() == 0 || nodeDepot == null)
            throw new Utils.WrongDocumentInput(new Throwable());

        Depot depot;

        Node node = nodeDepot.item(0);
        long idAddress = Long.parseLong(node.getAttributes().getNamedItem("address").getNodeValue());
        String departureTimeString = node.getAttributes().getNamedItem("departureTime").getNodeValue();

        HashMap<Long, Intersection> intersectionHashMap = MapProcessing.getIntersectionsHashMap();
        Intersection intersection = intersectionHashMap.get(idAddress);
        Date departureDate = null;
        try {
            departureDate = new SimpleDateFormat("H:mm:ss").parse(departureTimeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        depot = new Depot(departureDate, intersection);
        int pickupDuration;
        int deliveryDuration;
        long idPickupAddress;
        long idDeliveryAddress;

        List<Request> requestsList = new ArrayList<>();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            idPickupAddress = Long.parseLong(nNode.getAttributes().getNamedItem("pickupAddress").getNodeValue());
            idDeliveryAddress = Long.parseLong(nNode.getAttributes().getNamedItem("deliveryAddress").getNodeValue());
            pickupDuration = Integer.parseInt(nNode.getAttributes().getNamedItem("pickupDuration").getNodeValue());
            deliveryDuration = Integer.parseInt(nNode.getAttributes().getNamedItem("deliveryDuration").getNodeValue());

            requestsList.add(createRequest(idPickupAddress,idDeliveryAddress,pickupDuration,deliveryDuration,intersectionHashMap));
        }

        return new PlanningRequest(depot, requestsList);
    }

    /**
     * Create a request object
     * @param idPickupAddress pickup id
     * @param idDeliveryAddress delivery id
     * @param pickupDuration pickup time
     * @param deliveryDuration delivery time
     * @param intersectionHashMap map of intersections
     * @return a built request
     * @throws RequestToLargeException if intersection not in <code>intersectionHashMap</code>
     */
    public static Request createRequest(Long idPickupAddress, Long idDeliveryAddress,
                                        int pickupDuration, int deliveryDuration,
                                        HashMap<Long, Intersection> intersectionHashMap)
            throws RequestToLargeException {
        Intersection pickupAddress = intersectionHashMap.get(idPickupAddress);
        Intersection deliveryAddress = intersectionHashMap.get(idDeliveryAddress);

        if(pickupAddress == null || deliveryAddress == null){
            throw new RequestToLargeException(new Throwable());
        }

        return new Request(pickupDuration, deliveryDuration, pickupAddress, deliveryAddress);
    }

    /**
     * Exception related to wrong request file input
     */
    public static class RequestToLargeException extends Exception {
        /**
         * Constructor with stack trace
         * @param throwable stack trace
         */
        public RequestToLargeException(Throwable throwable) {
            super("Le fichier contient des requêtes vers des points non-présent sur la carte !", throwable);
        }
    }
}
