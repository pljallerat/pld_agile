package XML_processing;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * class containing static util methods
 */
public class Utils {

    /**
     * Parse an file to a {@link Document}
     * @param path path to the file to parse
     * @return a Document object (useful to read)
     * @throws IOException exception thrown by file reading
     * @throws SAXException thrown by xml reading
     * @throws ParserConfigurationException thrown by wrong configurations
     */
    public static Document parseXml(String path) throws IOException, SAXException, ParserConfigurationException {
        File xmlFile = new File(path);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse(xmlFile);
    }

    /**
     * Exception thrown if the input file isn't well wrote
     */
    public static class WrongDocumentInput extends Exception {

        /**
         * Constructor with stack trace
         * @param throwable trace
         */
        public WrongDocumentInput(Throwable throwable) {
            super("Le document sélectionné n'est pas au bon format !", throwable);
        }
    }

}
