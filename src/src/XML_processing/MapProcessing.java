package XML_processing;

import javax.xml.parsers.ParserConfigurationException;

import model.Intersection;
import model.Map;
import model.Segment;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static XML_processing.Utils.parseXml;

/**
 * Class used to read a Map file (xml)
 */
public class MapProcessing {

    private static Document doc;
    private static HashMap<Long, Intersection> intersectionsHashMap;
    private static Set<Segment> segmentsSet;
    private static float[] mapOffset;

    /**
     * Load the map contains in a xml file and generate a Model.Map object
     *
     * @param path path to the request XML file
     * @return a Model.Map
     * @throws IOException exception thrown by file reading
     * @throws SAXException thrown by xml reading
     * @throws ParserConfigurationException thrown by wrong configurations
     * @throws Utils.WrongDocumentInput     if the document doesn't contain what it is suppose to
     */
    public static Map manageMap(String path) throws ParserConfigurationException, SAXException, IOException, Utils.WrongDocumentInput {
        doc = parseXml(path);
        mapOffset = new float[4];
        intersectionsHashMap = generateIntersections(mapOffset);
        segmentsSet = generateSegments(intersectionsHashMap);
        return new Map(segmentsSet);
    }

    public static void setDoc(Document doc) {
        MapProcessing.doc = doc;
    }

    public static HashMap<Long, Intersection> getIntersectionsHashMap() {
        return intersectionsHashMap;
    }

    public static Set<Segment> getSegmentSet() {
        return segmentsSet;
    }

    public static float[] getMapOffset() {
        return mapOffset;
    }

    /**
     * Load the intersections of a map from a XML file.
     * The method fill a the "offset" tab in parameter to know latitude/longitude min/max
     *
     * @param offset a empty float tab which is going to be fill. index: 0->latitude max, 1->latitude min, 2->longitude max, 3->longitude min
     * @return a HashMap, the key is an Intersection id and the value is an intersection object
     */
    public static HashMap<Long, Intersection> generateIntersections(float[] offset) throws Utils.WrongDocumentInput {

        //liste des balises <intersection>
        NodeList nList = doc.getElementsByTagName("intersection");

        if (nList == null || nList.getLength() == 0)
            throw new Utils.WrongDocumentInput(new Throwable());

        float maxLat = Float.MIN_VALUE;
        float minLat = Float.MAX_VALUE;

        float minLong = Float.MAX_VALUE;
        float maxLong = Float.MIN_VALUE;

        float longitude;
        float latitude;
        long id;
        HashMap<Long, Intersection> intersectionMap = new HashMap<>();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            id = Long.parseLong(nNode.getAttributes().getNamedItem("id").getNodeValue());
            latitude = Float.parseFloat(nNode.getAttributes().getNamedItem("latitude").getNodeValue());
            longitude = Float.parseFloat(nNode.getAttributes().getNamedItem("longitude").getNodeValue());
            if (latitude > maxLat) maxLat = latitude;
            if (latitude < minLat) minLat = latitude;
            if (longitude > maxLong) maxLong = longitude;
            if (longitude < minLong) minLong = longitude;
            intersectionMap.put(id, new Intersection(id, latitude, longitude));
        }

        if (offset != null) {
            offset[0] = maxLat;
            offset[1] = minLat;
            offset[2] = maxLong;
            offset[3] = minLong;
        }


        return intersectionMap;
    }

    /**
     * Load the segments of a map from xml file
     *
     * @param intersectionMap a HashMap, the key is an Intersection id and the value is an intersection object
     * @return a set of Segment object
     */
    public static Set<Segment> generateSegments(HashMap<Long, Intersection> intersectionMap) throws Utils.WrongDocumentInput {

        //liste des balises <intersection>
        NodeList nList = doc.getElementsByTagName("segment");

        if (nList == null || nList.getLength() == 0)
            throw new Utils.WrongDocumentInput(new Throwable());

        long destinationId;
        long origineId;
        float length;
        String name;

        Set<Segment> segmentSet = new HashSet<>();

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            destinationId = Long.parseLong(nNode.getAttributes().getNamedItem("destination").getNodeValue());
            origineId = Long.parseLong(nNode.getAttributes().getNamedItem("origin").getNodeValue());
            name = nNode.getAttributes().getNamedItem("name").getNodeValue();
            length = Float.parseFloat(nNode.getAttributes().getNamedItem("length").getNodeValue());

            segmentSet.add(createSegment(origineId, destinationId, name, length, intersectionMap));
        }

        return segmentSet;
    }

    /**
     * Create a segment object
     * @param originId origin intersection id
     * @param destinationId destination intersection id
     * @param name segment name
     * @param length segment length
     * @param intersectionMap map of intersections
     * @return Built segment
     */
    public static Segment createSegment(Long originId, Long destinationId, String name, float length, HashMap<Long, Intersection> intersectionMap) {
        Intersection intersectionOrigin = intersectionMap.get(originId);
        Intersection intersectionDestination = intersectionMap.get(destinationId);

        return new Segment(length, name, intersectionOrigin, intersectionDestination);
    }
}