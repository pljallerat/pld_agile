package XML_processing;

import model.Intersection;
import model.PlanningRequest;
import model.Request;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.*;

public class RequestProcessingTest {

    @Test
    public final void createRequestTest(){
        HashMap<Long, Intersection> intersectionHashMap = new HashMap<>();
        intersectionHashMap.put(0L,new Intersection(0L,2,2));
        intersectionHashMap.put(1L,new Intersection(1L,1,2));
        intersectionHashMap.put(2L,new Intersection(2L,2,3));
        Request request = null;
        try {
            request = RequestProcessing.createRequest(0L,2L,200,180,intersectionHashMap);
        } catch (RequestProcessing.RequestToLargeException e) {
            e.printStackTrace();
        }
        assertNotNull(request);
        assertEquals(0L, (long) request.getPickupAddress().getId());
        assertEquals(02L, (long) request.getDeliveryAddress().getId());
        assertEquals(200,request.getPickupDuration());
        assertEquals(180,request.getDeliveryDuration());
    }

    @Test
    public final void createRequestExceptionTest(){
        HashMap<Long, Intersection> intersectionHashMap = new HashMap<>();
        intersectionHashMap.put(0L,new Intersection(0L,2,2));
        intersectionHashMap.put(1L,new Intersection(1L,1,2));
        intersectionHashMap.put(2L,new Intersection(2L,2,3));
        assertThrows(RequestProcessing.RequestToLargeException.class,() -> {
            RequestProcessing.createRequest(4L,1L,100,10, intersectionHashMap);
        });
    }

    @Test
    public final void generateRequestExceptionTest(){
        try {
            MapProcessing.manageMap("../data/mediumMap.xml");
        } catch (ParserConfigurationException | SAXException | IOException | Utils.WrongDocumentInput e) {
            e.printStackTrace();
        }
        assertThrows(Utils.WrongDocumentInput.class,() -> {
            RequestProcessing.generateRequestList("../data/smallMap.xml");
        });
    }

    @Test
    public final void generateRequestListTest(){
        PlanningRequest pr = null;
        try {
            MapProcessing.manageMap("../data/testMap.xml");
            pr = RequestProcessing.generateRequestList("../data/requestsTest.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(pr);

        assertEquals(2,pr.getRequests().size());
        assertEquals(00L, (long) pr.getDepot().getAddress().getId());
        assertEquals(240,pr.getRequests().get(1).getDeliveryDuration());
    }


}
