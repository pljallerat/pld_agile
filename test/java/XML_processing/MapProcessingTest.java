package XML_processing;

import model.Intersection;
import model.Map;
import model.Segment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;
import java.util.HashMap;
import java.util.Set;

public class MapProcessingTest {

    @Test
    public final void generateIntersectionsTest(){
        try {
            MapProcessing.setDoc(Utils.parseXml("../data/testMap.xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        float[] offset = new float[4];
        HashMap<Long, Intersection> intersectionsHashMap = null;
        try {
            intersectionsHashMap = MapProcessing.generateIntersections(offset);
        } catch (Utils.WrongDocumentInput wrongDocumentInput) {
            wrongDocumentInput.printStackTrace();
        }

        assertEquals(3,offset[0]);
        assertEquals(0,offset[1]);
        assertEquals(3,offset[2]);
        assertEquals(0,offset[3]);

        assertEquals(16,intersectionsHashMap.size());
        assertEquals(02,(long) intersectionsHashMap.get(02L).getId());
        assertEquals(2,intersectionsHashMap.get(02L).getLongitude());
        assertEquals(0,intersectionsHashMap.get(02L).getLatitude());
    }

    @Test
    public final void createRequestExceptionTest(){
        try {
            MapProcessing.setDoc(Utils.parseXml("../data/requestsSmall1.xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        float[] offset = new float[4];
        assertThrows(Utils.WrongDocumentInput.class,() -> {
            HashMap<Long, Intersection> intersectionsHashMap = MapProcessing.generateIntersections(offset);
        });


    }

    @Test
    public final void createSegmentTest(){
        HashMap<Long, Intersection> intersectionHashMap = new HashMap<>();
        intersectionHashMap.put(0L,new Intersection(0L,2,2));
        intersectionHashMap.put(1L,new Intersection(1L,1,2));
        intersectionHashMap.put(2L,new Intersection(2L,2,3));
        Segment segment = MapProcessing.createSegment(0L,2L,"Rue Danton",201.2f,intersectionHashMap);
        assertNotNull(segment);
        assertEquals(0L, (long) segment.getOrigin().getId());
        assertEquals(02L, (long) segment.getDestination().getId());
        assertEquals("Rue Danton",segment.getName());
        assertEquals(201.2f,segment.getLength());
    }

    @Test
    public final void manageMapTest(){
        Map map = null;
        try {
            map = MapProcessing.manageMap("../data/testMap.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(map);

        Set<Segment> segmentSet = map.getSegments();

        assertEquals(14,segmentSet.size());

        for(Segment s : segmentSet){
            assertNotNull(s);
            assertNotNull(s.getOrigin());
            assertNotNull( s.getDestination());
        }
    }

    @Test
    public final void manageMapWrongInputTest(){
        assertThrows(Utils.WrongDocumentInput.class, () -> MapProcessing.manageMap("../data/requestsSmall1.xml"));
    }

    @Test
    public final void manageMapWrongInputTest2(){
        assertThrows(Utils.WrongDocumentInput.class, () -> MapProcessing.manageMap("../data/testMap2.xml"));
    }
}
