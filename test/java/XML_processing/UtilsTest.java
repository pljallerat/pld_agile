package XML_processing;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UtilsTest {
    @Test
    public final void parseXmlTest(){
        Document doc = null;
        try {
            doc = Utils.parseXml("../data/testMap.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(doc);

        NodeList iList = doc.getElementsByTagName("intersection");
        assertEquals(16,iList.getLength());
        Node n = iList.item(0);
        assertEquals(0,Float.parseFloat(n.getAttributes().getNamedItem("latitude").getNodeValue()));
        assertEquals(0,Float.parseFloat(n.getAttributes().getNamedItem("longitude").getNodeValue()));
        assertEquals(00,Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue()));

        NodeList sList = doc.getElementsByTagName("segment");
        assertEquals(14,sList.getLength());
        n = sList.item(0);
        assertEquals(01,Long.parseLong(n.getAttributes().getNamedItem("destination").getNodeValue()));
        assertEquals(00,Long.parseLong(n.getAttributes().getNamedItem("origin").getNodeValue()));
        assertEquals("Rue Danton",n.getAttributes().getNamedItem("name").getNodeValue());
        assertEquals(1,Float.parseFloat(n.getAttributes().getNamedItem("length").getNodeValue()));
    }
}
