package algorithm.model;

import controller.Debug;
import model.Depot;
import model.Intersection;
import model.PlanningRequest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

public class StateHistoryTest {

    private static State newTemplateState(){
        return new State(
                new ArrayList<>(),
                new ArrayList<>(),
                new LinkedList<>(),
                new PlanningRequest(
                        new Depot(
                                new Date(0L), new Intersection(0L, 0,0)
                        ),
                        new ArrayList<>()
                )
        );
    }

    @Test
    void addModificationTest() {
        State currentState = newTemplateState();

        StateHistory history = new StateHistory();
        history.addModification(currentState);

        assertSame(history.getCurrentState(), currentState);

        State newState = newTemplateState();
        history.addModification(newState);
        assertSame(history.getCurrentState(), newState);
    }

    @Test
    void undoTest() {
        State currentState = newTemplateState();
        State newState = newTemplateState();

        StateHistory history = new StateHistory();
        history.addModification(currentState);
        history.addModification(newState);

        history.undo();
        assertSame(history.getCurrentState(), currentState);
    }

    @Test
    void redoTest() {
        State currentState = newTemplateState();
        State newState = newTemplateState();

        StateHistory history = new StateHistory();
        history.addModification(currentState);
        history.addModification(newState);

        history.undo();
        history.redo();
        assertSame(history.getCurrentState(), newState);
    }

    @Test
    void scenarioUndoRedoTest() {
        // Try to do A, do B, undo B, do C, undo C and undo A

        State A = newTemplateState();
        State B = newTemplateState();
        State C = newTemplateState();
        StateHistory history = new StateHistory();

        history.addModification(A);
        assertSame(A, history.getCurrentState());

        history.addModification(B);
        assertSame(B, history.getCurrentState());

        history.undo();
        assertSame(A, history.getCurrentState());

        history.addModification(C);
        assertSame(C, history.getCurrentState());

        history.undo();
        assertSame(A, history.getCurrentState());
    }

    @Test
    void limitCasesTest(){
        StateHistory history = new StateHistory();

        assertNull(history.getCurrentState());
        history.undo();
        assertNull(history.getCurrentState());
        history.redo();
        assertNull(history.getCurrentState());

        State A = newTemplateState();
        history.addModification(A);
        assertSame(A, history.getCurrentState());
        history.undo();
        assertSame(A, history.getCurrentState());
        history.redo();
        assertSame(A, history.getCurrentState());
    }

    @Test
    void clearHistoryTest() {
        StateHistory history = new StateHistory();
        State A = newTemplateState();
        State B = newTemplateState();
        State C = newTemplateState();
        history.addModification(A);
        history.addModification(B);
        history.addModification(C);
        history.undo();
        history.undo();
        history.redo();

        history.clearHistory();

        assertFalse(history.hasState());
        assertNull(history.getCurrentState());
    }
}
