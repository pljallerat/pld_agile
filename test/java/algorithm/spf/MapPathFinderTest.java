package algorithm.spf;

import algorithm.model.Path;
import javafx.util.Pair;
import model.*;
import model.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MapPathFinderTest {

    private static Map map = null;
    private static MapPathFinder mpf;
    private static Intersection[] intersections = new Intersection[5];

    private static float p2(float distance) {
        return distance * distance;
    }

    /*
       . . 2-5 . .
       . . | |\. .
       . . . . . .
       . ././. | .
       . 4 . . . .
       . |/. . .\.
       . 1 . . . 3 
     
     */

    @BeforeAll
    public static void init() {
        intersections[0] = new Intersection(0L, 1, 1);
        intersections[1] = new Intersection(1L, 2, 4);
        intersections[2] = new Intersection(2L, 5, 1);
        intersections[3] = new Intersection(3L, 1, 2);
        intersections[4] = new Intersection(4L, 3, 4);

        Set<Segment> segments = new HashSet<>();

        float length = (float) Math.sqrt(p2(intersections[0].getLatitude() - intersections[3].getLatitude()) +
                p2(intersections[0].getLongitude() - intersections[3].getLongitude()));
        segments.add(new Segment(length, "a", intersections[0], intersections[3]));
        segments.add(new Segment(length, "a", intersections[3], intersections[0]));

        length = (float) Math.sqrt(p2(intersections[3].getLatitude() - intersections[1].getLatitude()) +
                p2(intersections[3].getLongitude() - intersections[1].getLongitude()));
        segments.add(new Segment(length, "a", intersections[3], intersections[1]));
        segments.add(new Segment(length, "a", intersections[1], intersections[3]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[1].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[1].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[1]));
        segments.add(new Segment(length, "a", intersections[1], intersections[4]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[0].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[0].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[0]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[2].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[2].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[2]));
        segments.add(new Segment(length, "a", intersections[2], intersections[4]));

        map = new Map(segments);

        mpf = new MapPathFinder(map, new ArrayList<>(Arrays.asList(intersections)));
    }

    @Test
    public void constructorTest() {
        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, intersections[2], intersections[3]));

        Depot depot = new Depot(new Date(0), intersections[1]);
        PlanningRequest request = new PlanningRequest(depot, requests);

        MapPathFinder mpf = new MapPathFinder(map, request);

        assertNotNull(mpf);

        assertEquals(5, mpf.getVerticesMap().size());

        assertEquals(1, mpf.getVerticesMap().get(0L).getAdjacencyList().size());
    }

    @Test
    public void findShorterPathsTest() {
        init();
        java.util.Map<Pair<Intersection, Intersection>, Path> shorterPath = mpf.findShorterPaths();

        assertNotNull(shorterPath);
        assertEquals(5 * 4, shorterPath.size()); // n* n-1

        // Lets try some 
        assertEquals(1,
                shorterPath.get(new Pair<>(intersections[0], intersections[3]))
                        .getTotalLength());


        assertEquals((float) Math.sqrt(2 * 2 + 3 * 3),
                shorterPath.get(new Pair<>(intersections[4], intersections[0]))
                        .getTotalLength());

        assertEquals((float) (1 + Math.sqrt(1 + 2 * 2) + 1 + Math.sqrt(2 * 2 + 3 * 3)),
                shorterPath.get(new Pair<>(intersections[0], intersections[2]))
                        .getTotalLength());
    }
}
