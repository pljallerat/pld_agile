package algorithm.spf;

import algorithm.model.Vertex;
import algorithm.spf.Dijkstra;
import model.Intersection;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DijkstraTest {

    @Test
    public final void ClassTest(){

        Vertex[] vertices = new Vertex[5];
        vertices[0] = new Vertex(new Intersection(0L,1,1));
        vertices[1] = new Vertex(new Intersection(1L,1,1));
        vertices[2] = new Vertex(new Intersection(2L,1,1));
        vertices[3] = new Vertex(new Intersection(3L,1,1));
        vertices[4] = new Vertex(new Intersection(4L,1,1));

        vertices[0].addNeighbor(vertices[2],3f);
        vertices[0].addNeighbor(vertices[3],2f);
        vertices[1].addNeighbor(vertices[2],1f);
        vertices[2].addNeighbor(vertices[1],1f);
        vertices[2].addNeighbor(vertices[0],3f);
        vertices[2].addNeighbor(vertices[3],1f);
        vertices[3].addNeighbor(vertices[4],2f);
        vertices[3].addNeighbor(vertices[0],2f);
        vertices[4].addNeighbor(vertices[3],2f);

        Dijkstra d = new Dijkstra(vertices);
        d.start(0);

        assertNotNull(d.getVertices());
        assertEquals(3L,  (long) d.getVertices()[4].getPredecessor().getIntersection().getId());
        assertEquals(4f,d.getVertices()[4].getCurrentDistance());
        assertEquals(2L, (long) d.getVertices()[1].getPredecessor().getIntersection().getId());
        assertEquals(4f,d.getVertices()[1].getCurrentDistance());
    }
}
