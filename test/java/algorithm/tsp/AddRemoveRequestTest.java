package algorithm.tsp;

import algorithm.model.State;
import controller.Debug;
import javafx.util.Pair;
import model.*;
import model.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class AddRemoveRequestTest {

    private static Map map = null;
    private static Intersection[] intersections = new Intersection[5];

    private static float p2(float distance) {
        return distance * distance;
    }

    /*
       . . 2-5 . .
       . . | |\. .
       . . . . . .
       . ././. | .
       . 4 . . . .
       . |/. . .\.
       . 1 . . . 3

     */

    @BeforeAll
    public static void init() {
        intersections[0] = new Intersection(0L, 1, 1);
        intersections[1] = new Intersection(1L, 2, 4);
        intersections[2] = new Intersection(2L, 5, 1);
        intersections[3] = new Intersection(3L, 1, 2);
        intersections[4] = new Intersection(4L, 3, 4);

        Set<Segment> segments = new HashSet<>();

        float length = (float) Math.sqrt(p2(intersections[0].getLatitude() - intersections[3].getLatitude()) +
                p2(intersections[0].getLongitude() - intersections[3].getLongitude()));
        segments.add(new Segment(length, "a", intersections[0], intersections[3]));
        segments.add(new Segment(length, "a", intersections[3], intersections[0]));

        length = (float) Math.sqrt(p2(intersections[3].getLatitude() - intersections[1].getLatitude()) +
                p2(intersections[3].getLongitude() - intersections[1].getLongitude()));
        segments.add(new Segment(length, "a", intersections[3], intersections[1]));
        segments.add(new Segment(length, "a", intersections[1], intersections[3]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[1].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[1].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[1]));
        segments.add(new Segment(length, "a", intersections[1], intersections[4]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[0].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[0].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[0]));

        length = (float) Math.sqrt(p2(intersections[4].getLatitude() - intersections[2].getLatitude()) +
                p2(intersections[4].getLongitude() - intersections[2].getLongitude()));
        segments.add(new Segment(length, "a", intersections[4], intersections[2]));
        segments.add(new Segment(length, "a", intersections[2], intersections[4]));

        map = new Map(segments);
    }

    @Test
    void addRequestTest() {
        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, intersections[2], intersections[3]));

        Depot depot = new Depot(new Date(0), intersections[1]);
        PlanningRequest request = new PlanningRequest(depot, requests);

        FindPath findPath = new FindPath(map, request, (state, timeLimit) -> {
        });

        State state = findPath.findPath();

        // Start testing :

        Request newRequest = new Request(10, 20, intersections[0], intersections[4]);
        State newState = findPath.addRequest(map, state, newRequest);

        // Assert that new planning request contains delivery request
        assertTrue(newState.planningRequest.getRequests().contains(newRequest));

        // Assert that every arrival times aren't modified (except the last : depot)
        for (int i = 0; i < state.arrivalTimes.size() - 1; i++) {
            assertTrue(
                    newState.arrivalTimes.contains(
                            state.arrivalTimes.get(i)));
        }

        // Assert that order contains every useful intersection, in the right order
        assertArrayEquals(
                newState.orderedListOfUsefulIntersections.toArray(new Intersection[]{}),
                new Intersection[]{
                        depot.getAddress(),
                        intersections[2],
                        intersections[3],
                        intersections[0],
                        intersections[4],
                        depot.getAddress()
                }
        );

        // Assert that order contains every useful intersection, in the right order
        assertArrayEquals(
                newState.path.toArray(new Intersection[]{}),
                new Intersection[]{
                        depot.getAddress(),
                        intersections[4],
                        intersections[2],
                        intersections[2], //the useful point are duplicated
                        intersections[4],
                        intersections[1],
                        intersections[3],
                        intersections[3],
                        intersections[0],
                        intersections[0],
                        intersections[3],
                        intersections[1],
                        intersections[4],
                        intersections[4],
                        depot.getAddress()
                }
        );
    }

    @Test
    void removeRequestTest() {
        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, intersections[2], intersections[4]));
        requests.add(new Request(0, 0, intersections[0], intersections[3]));

        Depot depot = new Depot(new Date(0), intersections[1]);
        PlanningRequest request = new PlanningRequest(depot, requests);

        FindPath findPath = new FindPath(map, request, (state, timeLimit) -> {
        });

        State state = findPath.findPath();

        Debug.i("Test", "path found : " + Debug.toString(state.path, true));

        // Remove 1st request
        State newState = findPath.removeRequest(state, requests.get(0));

        // asserts
        assertFalse(newState.planningRequest.getRequests().contains(requests.get(0)));

        // Arrivals times shouldn't change
        for (Pair<Long, Intersection> lastArrivalTimes : newState.arrivalTimes) {
            for (Pair<Long, Intersection> newArrivalTimes : state.arrivalTimes) {
                if (lastArrivalTimes.getValue().equals(newArrivalTimes.getValue())) {
                    assertEquals(lastArrivalTimes.getKey(), newArrivalTimes.getKey());
                }
            }
        }

        // Right order
        assertArrayEquals(
                newState.orderedListOfUsefulIntersections.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[0],
                        intersections[3],
                        intersections[1]
                }
        );

        // Right path
        assertArrayEquals(
                newState.path.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[3],
                        intersections[0],
                        intersections[0],
                        intersections[3],
                        intersections[3],
                        intersections[1]
                }
        );
    }

    @Test
    void removeLastRequestTest() {
        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, intersections[2], intersections[4]));
        requests.add(new Request(0, 0, intersections[0], intersections[3]));

        Depot depot = new Depot(new Date(0), intersections[1]);
        PlanningRequest request = new PlanningRequest(depot, requests);

        FindPath findPath = new FindPath(map, request, (state, timeLimit) -> {
        });

        State state = findPath.findPath();

        // Remove
        State newState = findPath.removeRequest(state, request.getRequests().get(1));

        // asserts
        assertFalse(newState.planningRequest.getRequests().contains(requests.get(1)));

        // Arrivals times shouldn't change
        for (Pair<Long, Intersection> lastArrivalTimes : newState.arrivalTimes) {
            for (Pair<Long, Intersection> newArrivalTimes : state.arrivalTimes) {
                if (lastArrivalTimes.getValue().equals(newArrivalTimes.getValue())) {
                    if(lastArrivalTimes.getValue().equals(depot.getAddress())){
                        // Arrival times should change
                        assertNotEquals(lastArrivalTimes.getKey(), newArrivalTimes.getKey());
                    } else {
                        // Other times should stay
                        assertEquals(lastArrivalTimes.getKey(), newArrivalTimes.getKey());
                    }
                }
            }
        }

        // Right order
        assertArrayEquals(
                newState.orderedListOfUsefulIntersections.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[2],
                        intersections[4],
                        intersections[1]
                }
        );

        // Right path
        assertArrayEquals(
                newState.path.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[4],
                        intersections[2],
                        intersections[2],
                        intersections[4],
                        intersections[4],
                        intersections[1]
                }
        );
    }

    @Test
    void removeThenAddRequestTest() {
        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, intersections[2], intersections[4]));
        requests.add(new Request(0, 0, intersections[0], intersections[3]));

        Depot depot = new Depot(new Date(0), intersections[1]);
        PlanningRequest request = new PlanningRequest(depot, requests);

        FindPath findPath = new FindPath(map, request, (state, timeLimit) -> {
        });

        State state = findPath.findPath();

        State intermediate = findPath.removeRequest(state, requests.get(0));

        final Request toAdd = new Request(0, 0, intersections[4], intersections[2]);
        State newState = findPath.addRequest(map, intermediate, toAdd);

        // Asserts
        assertFalse(newState.planningRequest.getRequests().contains(requests.get(0)));
        assertTrue(newState.planningRequest.getRequests().contains(toAdd));

        // Arrivals times shouldn't change
        for (Pair<Long, Intersection> lastArrivalTimes : newState.arrivalTimes) {
            for (Pair<Long, Intersection> newArrivalTimes : state.arrivalTimes) {
                if (lastArrivalTimes.getValue().equals(newArrivalTimes.getValue())) {
                    if(lastArrivalTimes.getValue().equals(intersections[2]) ||
                            lastArrivalTimes.getValue().equals(intersections[4])){
                        // Those are modified : times should change
                        assertNotEquals(lastArrivalTimes.getKey(), newArrivalTimes.getKey());
                    } else {
                        // Other times should stay
                        assertEquals(lastArrivalTimes.getKey(), newArrivalTimes.getKey());
                    }
                }
            }
        }

        // Right order
        assertArrayEquals(
                newState.orderedListOfUsefulIntersections.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[4],
                        intersections[2],
                        intersections[0],
                        intersections[3],
                        intersections[1]
                }
        );

        // Right path
        assertArrayEquals(
                newState.path.toArray(new Intersection[]{}),
                new Intersection[]{
                        intersections[1],
                        intersections[4],
                        intersections[4],
                        intersections[2],
                        intersections[2],
                        intersections[4],
                        intersections[0],
                        intersections[0],
                        intersections[3],
                        intersections[3],
                        intersections[1]
                }
        );
    }
}
