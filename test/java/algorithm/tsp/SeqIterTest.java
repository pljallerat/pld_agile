package algorithm.tsp;

import algorithm.model.Graph;
import javafx.util.Pair;
import model.Depot;
import model.Intersection;
import model.PlanningRequest;
import model.Request;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SeqIterTest {

    @Test
    public final void testReduceMatrice(){
        //https://www.youtube.com/watch?v=1FEP_sNb62k&ab_channel=AbdulBari
        float [][] matrice = {{-1,20,30,10,11},{15,-1,16,4,2},{3,5,-1,2,4},{19,6,18,-1,3},{16,4,7,16,-1}};
        Pair<Float, Float[][]> resultat = SeqIter.reduceMatrice(matrice,5);
        assertEquals(25, (float) resultat.getKey());

        float[][] reduced = float[][].class.cast(resultat.getValue());
        float[][] expected = {{-1,10,17,0,1},{12,-1,11,2,0},{0,3,-1,0,2},{15,3,12,-1,0},{11,0,0,12,-1}};

        for(int i=0;i<5;i++){
            for(int j=0; j<5; j++){
                assertEquals(expected[i][j],reduced[i][j]);
            }
            System.out.println();
        }
    }

    @Test
    public final void testbound1(){
        //Test dans le premier cas où currentVertex et precedentVertex sont le même
        float [][] matrice = {{-1,20,30,10,11},{15,-1,16,4,2},{3,5,-1,2,4},{19,6,18,-1,3},{16,4,7,16,-1}};
        Pair<Float, Float[][]> resultat = SeqIter.bound(0,0,5,matrice,10);
        assertEquals(25, (float) resultat.getKey());

        float[][] returned = float[][].class.cast(resultat.getValue());
        float[][] expected = {{-1,10,17,0,1},{12,-1,11,2,0},{0,3,-1,0,2},{15,3,12,-1,0},{11,0,0,12,-1}};

        for(int i=0;i<5;i++){
            for(int j=0; j<5; j++){
                assertEquals(expected[i][j],returned[i][j]);
            }
        }
    }

    @Test
    public final void testbound2(){
        //Test dans le premier cas où currentVertex et precedentVertex ne sont pas le même
        float [][] matrice = {{-1,20,30,10,11},{15,-1,16,4,2},{3,5,-1,2,4},{19,6,18,-1,3},{16,4,7,16,-1}};
        Pair<Float, Float[][]> resultat = SeqIter.bound(1,2,5,matrice,10);
        assertEquals(46, (float) resultat.getKey());

        float[][] returned = float[][].class.cast(resultat.getValue());
        float[][] expected = {{-1,-1,20,0,1},{4,-1,-1,2,0},{-1,-1,-1,-1,-1},{7,-1,15,-1,0},{0,-1,0,9,-1}};

        for(int i=0;i<5;i++){
            for(int j=0; j<5; j++){
                assertEquals(expected[i][j],returned[i][j]);
            }
        }
    }


}
