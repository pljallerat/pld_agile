package algorithm.tsp;

import javafx.util.Pair;
import model.*;
import org.junit.jupiter.api.Test;

import java.util.*;

public class AlgorithmTest {



    @Test
    public final void testCompleteGraph() {
        //Pair<Map<Pair<Intersection, Intersection>, Path>, PlanningRequest> jeuTest = getJeuTest();
        //Graph cp = new CompleteGraph(jeuTest.getKey(),jeuTest.getValue());
        //assertEquals(1,1);
        //System.out.println(cp.getNbVertices());
    }

    public Pair<model.Map, PlanningRequest> getJeuTest() {
        Intersection i1 = new Intersection(0L, 1, 1);
        Intersection i2 = new Intersection(1L, 2, 4);
        Intersection i3 = new Intersection(2L, 5, 1);
        Intersection i4 = new Intersection(3L, 1, 2);
        Intersection i5 = new Intersection(4L, 3, 4);

        Set<Segment> segments = new HashSet<>();

        float length = (float) Math.sqrt(p2(i1.getLatitude() - i4.getLatitude()) +
                p2(i1.getLongitude() - i4.getLongitude()));
        segments.add(new Segment(length, "a", i1, i4));
        segments.add(new Segment(length, "a", i4, i1));

        length = (float) Math.sqrt(p2(i4.getLatitude() - i2.getLatitude()) +
                p2(i4.getLongitude() - i2.getLongitude()));
        segments.add(new Segment(length, "a", i4, i2));
        segments.add(new Segment(length, "a", i2, i4));

        length = (float) Math.sqrt(p2(i5.getLatitude() - i2.getLatitude()) +
                p2(i5.getLongitude() - i2.getLongitude()));
        segments.add(new Segment(length, "a", i5, i2));
        segments.add(new Segment(length, "a", i2, i5));

        length = (float) Math.sqrt(p2(i5.getLatitude() - i1.getLatitude()) +
                p2(i5.getLongitude() - i1.getLongitude()));
        segments.add(new Segment(length, "a", i5, i1));
        segments.add(new Segment(length, "a", i1, i5));

        length = (float) Math.sqrt(p2(i5.getLatitude() - i3.getLatitude()) +
                p2(i5.getLongitude() - i3.getLongitude()));
        segments.add(new Segment(length, "a", i5, i3));
        segments.add(new Segment(length, "a", i3, i5));


        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, i3, i4));


        model.Map map = new model.Map(segments);

        Depot depot = new Depot(new Date(0), i2);
        PlanningRequest request = new PlanningRequest(depot, requests);

        return new Pair<>(map, request);
    }

    private static float p2(float distance) {
        return distance * distance;
    }
}
