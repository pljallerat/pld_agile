package algorithm.tsp;

import XML_processing.MapProcessing;
import XML_processing.RequestProcessing;
import algorithm.model.Graph;
import algorithm.model.Path;
import algorithm.spf.MapPathFinder;
import controller.Debug;
import javafx.util.Pair;
import model.Intersection;
import model.Map;
import model.PlanningRequest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TemplateTSPTest {

    @Test
    public final void searchSolutionTest(){
        Map m = null;
        PlanningRequest p = null;
        try {
            m = MapProcessing.manageMap("../data/testMap.xml");
            p = RequestProcessing.generateRequestList("../data/requestsTest.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }

        MapPathFinder pathFinder = new MapPathFinder(m, p);
        java.util.Map<Pair<Intersection, Intersection>, Path> shorterPath = pathFinder.findShorterPaths();
        Graph g = new CompleteGraph(shorterPath, p);
        TemplateTSP tsp = new TemplateTSP();
        tsp.searchSolution(20000, g);

        assertEquals(13.199999f,tsp.getSolutionCost());
        assertEquals(0, (long) tsp.getSolution(0));
        assertEquals(1, (long) tsp.getSolution(1));
        assertEquals(3, (long) tsp.getSolution(2));
        assertEquals(4, (long) tsp.getSolution(3));
        assertEquals(2, (long) tsp.getSolution(4));
    }

    @Test
    void limitCasesGettersTest() {
        TemplateTSP test = new TemplateTSP();
        assertEquals(test.getSolution(0),Integer.valueOf(-1));
        assertEquals(test.getSolutionCost(),-1);
    }
}
