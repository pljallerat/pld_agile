package algorithm.tsp;

import algorithm.model.Path;
import javafx.util.Pair;
import model.Depot;
import model.Intersection;
import model.PlanningRequest;
import model.Request;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompleteGraphTest {

    CompleteGraph graph;

    public final void init(){
        Intersection a = new Intersection(0L, 1, 1);
        Intersection b = new Intersection(1L, 2, 4);
        Intersection c = new Intersection(2L, 5, 1);
        java.util.Map<Pair<Intersection, Intersection>, Path> carte = new HashMap<Pair<Intersection, Intersection>, Path>();

        Pair<Intersection, Intersection> ab = new Pair<>(a, b);
        Pair<Intersection, Intersection> ba = new Pair<>(b, a);
        Pair<Intersection, Intersection> bc = new Pair<>(b, c);
        Pair<Intersection, Intersection> cb = new Pair<>(c, b);
        Pair<Intersection, Intersection> ca = new Pair<>(c, a);
        Pair<Intersection, Intersection> ac = new Pair<>(a, c);

        carte.put(ab,new Path(null,1));
        carte.put(ba,new Path(null,1));
        carte.put(bc,new Path(null,2));
        carte.put(cb,new Path(null,2));
        carte.put(ca,new Path(null,3));
        carte.put(ac,new Path(null,3));

        List<Request> requests = new ArrayList<>();
        requests.add(new Request(0, 0, b, c));

        Depot depot = new Depot(new Date(0), a);
        PlanningRequest request = new PlanningRequest(depot, requests);

        graph = new CompleteGraph(carte,request);
    }

    @Test
    public final void testCreateMatrice(){
        init();
        float[][] calcul = graph.createMatrice();
        float[][] theorie = {{-1,1,3},{1,-1,2},{3,2,-1}};

        for(int i =0;i<3;i++){
            for(int j=0;j<3;j++){
                assertEquals(calcul[i][j],theorie[i][j]);
            }
        }
    }

    @Test
    public final void testIsArc(){
        init();
        assertEquals(false, graph.isArc(-1,0));
        assertEquals(false, graph.isArc(0,-1));
        assertEquals(false, graph.isArc(9999999,0));
        assertEquals(false, graph.isArc(0,9999999));
        assertEquals(false, graph.isArc(2,2));
        assertEquals(true, graph.isArc(1,2));
    }

    @Test
    public final void testGetCost(){
        init();
        assertEquals(-1, graph.getCost(-1,0));
        assertEquals( 2, graph.getCost(1,2));
    }
}
